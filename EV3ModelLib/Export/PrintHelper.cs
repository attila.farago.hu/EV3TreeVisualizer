﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EV3ModelLib
{
    public static class PrintHelper
    {
        #region Printing
        const bool IsOptimizeParallelLastBlock = false;
        public static string GetPMDListString(this Node node)
        {
            var isParentLastChildArr = new bool[] { };
            return GetPMDListString_priv(node, 0, isParentLastChildArr, true);
        }
        private static string GetPMDListString_priv(this Node lpmd, int depth, bool[] isParentLastChildArr, bool isThisLastKid)
        {
            StringBuilder sbretval = new StringBuilder();

            bool[] isParentLastChildArrNew = isParentLastChildArr;

            // No need for ForkParent nodes as with ev3 compilation there is no code after TRIG/FORK statements
            // skip single block if it was the last FORK (no nodes follow, this is the longest one due to reordering)
            bool skipThisSingleBlock = false;
            if (IsOptimizeParallelLastBlock) if (lpmd.NodeType == Node.NodeTypeEnum.ForkItem && isThisLastKid) skipThisSingleBlock = true;

            // print line starting
            if (!skipThisSingleBlock)
            {
                StringBuilder sb = new StringBuilder();
                isParentLastChildArrNew = isParentLastChildArr.Concat(new bool[1] { isThisLastKid }).ToArray();
                //sb.Append(string.Join(".", isParentLastChildArrNew.ToArray()));
                if (PrintOptions.UseTreeChars)
                {
                    for (int i = 1; i < isParentLastChildArrNew.Length; i++)
                    {
                        bool aLastKidOnLevel = isParentLastChildArrNew[i];
                        bool isThisLastLevel = i == isParentLastChildArr.Length;
                        sb.Append(
                                (aLastKidOnLevel ?
                                        (isThisLastLevel ? " └" : "  ") :
                                        (isThisLastLevel ? " ├" : " │")));
                        sb.Append(
                            isThisLastLevel ? "─ " : " "
                            );
                    }
                }
                else
                {
                    sb.Append("".PadLeft((isParentLastChildArrNew.Length - 1) * 2));
                }

                string colorOverride = null;
                if (PrintOptions != null && PrintOptions.UseColoring && PrintOptions.pmdHighlight != null && PrintOptions.pmdHighlight.Contains(lpmd)) colorOverride = "Blue";
                sbretval.Append(StringWithColor(colorOverride, sb.ToString()));

                // print line content
                sbretval.AppendLine(lpmd.GetPmdPrintString(depth, colorOverride));
            }

            // print children
            foreach (var pmd in lpmd.Children)
                sbretval.Append(GetPMDListString_priv(pmd, depth + 1, isParentLastChildArrNew, lpmd.Children?.Last() == pmd));

            return sbretval.ToString();
        }
        private static string GetPmdPrintString(this Node node, int depth = 0, string colorOverride = null)
        {
            StringBuilder sb = new StringBuilder();
            {
                string color = colorOverride;
                if (PrintOptions != null && PrintOptions.UseColoring && color == null)
                {
                    if (depth == 0)
                    {
                        color = C(BlockFamilyColorizer.BlockColorMap[Node.BLOCK_Root]);
                        if (PrintOptions?.TargetColorIndex == PrintOptionsClass.TargetColorEnum.Html)
                            sb.Append($"<a id='{node.Name}'></a>");
                    }
                    else
                    {
                        if (BlockFamilyColorizer.BlockTypeColorMap.ContainsKey(node.NodeType)) color = C(BlockFamilyColorizer.BlockTypeColorMap[node.NodeType]);
                        else if (node.HasSwitch || node.HasWait) color = C(BlockFamilyColorizer.BlockColorSwitchAndWaitFor);
                        else if (BlockFamilyColorizer.BlockColorMap.TryGetValue(node.Name, out var colorRecord)) color = C(colorRecord);
                    }
                }

                //-- Node short Name                
                string nameStr = node.Name;

                //-- prefixes
                if (node.NodeType == Node.NodeTypeEnum.Loop) nameStr = Node.BLOCK_LoopPrefix + nameStr;
                else if (node.HasWait) nameStr = Node.BLOCK_WaitForPrefix + nameStr;
                else if (node.HasSwitch) nameStr = Node.BLOCK_SwitchPrefix + nameStr;

                sb.Append(StringWithColor(color, nameStr));
            }

            //if (this.TrackerGlobalStart >= 0)
            //    Console.Write($" | GLOBAL{this.TrackerGlobalStart}-GLOBAL{this.TrackerGlobalEnd}");
            //Console.ForegroundColor = ConsoleColor.DarkGray;
            if (node.Parameters.Count > 0)
            {
                string color = colorOverride;
                if (PrintOptions != null && PrintOptions.UseColoring && color == null) color = C(BlockFamilyColorizer.BlockColorMap["PARAMS"]);

                //-- any optional conversion for display
                sb.Append(StringWithColor(color, " " +
                    (PrintOptions.UseParamNames ?
                        string.Join(" | ",
                            node.Parameters.Select(elemkvp =>
                                $"{elemkvp.Key}: {elemkvp.Value.ValueFormatted}"
                            )
                        ) :
                        string.Join(", ",
                            node.Parameters.Select(
                                elemkvp => elemkvp.Value.ValueFormatted
                            )
                        )
                    )
                ));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Color code
        /// </summary>
        /// <param name="colorrecord"></param>
        /// <returns></returns>
        private static string C(string[] colorrecord)
        {
            return PrintOptions.TargetColorIndex >= 0 ?
                colorrecord[(int)PrintOptions.TargetColorIndex] :
                "Gray";
        }

        /// <summary>
        /// Colorize output content
        /// </summary>
        /// <param name="colorFront"></param>
        /// <param name="colorBack"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        /// <see cref="https://github.com/AlexGhiondea/OutputColorizer"/>
        internal static string StringWithColor(string colorFront, string content)
        {
            if (content == null) return null;

            if (colorFront != null)
            {
                switch (PrintOptions?.TargetColorIndex)
                {
                    case PrintOptionsClass.TargetColorEnum.Console:
                        content = content.Replace("[", @"\[").Replace("]", @"\]"); //TODO
                        return $"[{colorFront}!{content}]";
                    case PrintOptionsClass.TargetColorEnum.Html:
                        return $"<span style='color:{colorFront}'>{content}</span>";
                    default:
                        return content;
                }
            }
            else
            {
                return content;
            }
        }

        /// <summary>
        /// Add separator
        /// </summary>
        /// <returns></returns>
        public static string Separator()
        {
            return StringWithColor("DarkCyan", new String('=', 80));
        }

        public static PrintOptionsClass PrintOptions { get; set; } = new PrintOptionsClass();
        #endregion Printing

        public static bool In<T>(this T x, params T[] set)
        {
            return set.Contains(x);
        }

    }

    /// <summary>
    /// Print Options
    /// </summary>
    public class PrintOptionsClass
    {
        public enum TargetColorEnum
        {
            None = -1, Html = 0, Console = 1
        }
        public TargetColorEnum TargetColorIndex = TargetColorEnum.Console;
        public bool UseColoring { get { return TargetColorIndex != TargetColorEnum.None; } }
        public List<Node> pmdHighlight = null;
        public bool UseTreeChars = true;
        public bool UseParamNames = true;
        public bool SkipPrinting = false;
    }
}
