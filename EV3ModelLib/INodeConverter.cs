﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3ModelLib
{
    public interface INodeConverter
    {
        Node Convert(object source);
    }
}
