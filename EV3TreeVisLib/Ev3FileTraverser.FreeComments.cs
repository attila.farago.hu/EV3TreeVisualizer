﻿using EV3ModelLib;
using EV3TreeVisLib.Logic;
using EV3TreeVisLib.Logic.Converter;
using EV3TreeVisLib.Position;
using Medallion.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Wmhelp.XPath2;

namespace EV3TreeVisLib
{
    internal partial class Ev3FileTraverser
    {
        internal PositionCache Cache = new PositionCache();

        /// <summary>
        /// Extract freestanding comment blocks and associate them with the closest node
        /// </summary>
        /// <param name="rootNode"></param>
        private void PostProcessFreeStandingCommentBlocks(LogBlock rootNode, List<LogBlock> allnodes)
        {
            XElement rootNodeXml = rootNode.Element;
            Cache.Initialize();

            var commentsBoxes = new Dictionary<LogBlock, List<Tuple<Point, XElement>>>();
            var commentNodes = rootNodeXml.XPath2SelectElements(Options.IsEV3Desktop ? "//*:Comment" : "//*:X3Comment").ToList();
            //!! if (commentNodes.Count == 0) return; //TODO: optimization purposes, enter it again

            // Collect all other blocks that are connected (have a sequence in)
            var nodeLocations = new List<Tuple<Point, LogBlock>>();
            foreach (LogBlock othernode in allnodes)
            {
                // check valid node parent sequence wires
                //Bounds="83 12 100 80" x=83..83+100, y=12..12+80 --> x1 y1 width height
                Bounds bounds = Cache.BoundsLogBlock(othernode);
                //if there are no bounds but node is non-native - get bounds from parent
                if (bounds == null && !othernode.IsBlockTypeNative && othernode.Parent != null)
                    bounds = Cache.BoundsLogBlock(othernode.Parent);
                if (bounds == null) continue;
                // anchor point will be top left
                nodeLocations.Add(Tuple.Create<Point, LogBlock>(bounds.Anchor, othernode));
            }

            // Collect all freestanding Comment blocks
            foreach (XElement commentNode in commentNodes)
            {
                //Bounds="83 12 100 80" x=83..83+100, y=12..12+80 --> x1 y1 width height
                Bounds bounds = Cache.BoundsFromXElementWithShift(commentNode); //TODO: create LogBlock already here
                if (bounds == null) continue;

                // anchor point will be bottom left
                Point anchor = new Point(bounds.Anchor.X, bounds.Anchor.Y + bounds.Height);

                // CommenBoxes should track child-parent relationships(should only consider nodes under same "case" if have parent
                var nodeLocationsToConsider = nodeLocations;
                var parentElem = commentNode.XPath2SelectOne("..");
                LogBlock parentNode = allnodes.Where(elem => elem.Element == parentElem).FirstOrDefault();
                if (parentNode != null)
                {
                    var lbsNodesUnderParentNode = parentNode.ChildrenTraversed();
                    nodeLocationsToConsider = nodeLocations.Where(nl => nl.Item2 != parentNode && lbsNodesUnderParentNode.Contains(nl.Item2)).ToList();
                }

                //find closest node
                var closestNode = nodeLocationsToConsider.MinBy(elem =>
                {
                    var nodeloc = elem.Item1;
                    var commentloc = anchor;
                    return Math.Pow(nodeloc.X - commentloc.X, 2) + Math.Pow(nodeloc.Y - commentloc.Y, 2);
                });
                if (!commentsBoxes.ContainsKey(closestNode.Item2)) commentsBoxes[closestNode.Item2] = new List<Tuple<Point, XElement>>();
                commentsBoxes[closestNode.Item2].Add(Tuple.Create<Point, XElement>(anchor, commentNode));
            }

            // order freestanding comment blocks for one node (if more than one) by X
            foreach (var commentkvp in commentsBoxes.ToList()) //avoid modifying the list while iterating on it
            {
                if (commentkvp.Value.Count > 1)
                    commentsBoxes[commentkvp.Key] = commentkvp.Value.OrderBy(elem => elem.Item1.X).ToList();

                foreach (var commenttuple in commentsBoxes[commentkvp.Key])
                {
                    var commentNode = commenttuple.Item2;
                    var comment1 = commentNode.XPath2SelectOne(Options.IsEV3Desktop ? "string(*:Content/text())" : "string(*:p.Text/text())").ToString();

                    //add the comment block
                    comment1 = comment1.Trim(); //.Replace("\n", " ");
                    LogBlock lbCurrent = commentkvp.Key;
                    LogBlock lbComment = new LogBlock(commenttuple.Item2)
                    {
                        Parent = lbCurrent.Parent
                    };

                    lbComment.Block = LogBlock.ExtraBlocks.COMMENTBOX;
                    lbComment.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(BlockNameConverter.CONST_BlockTarget_CommentBlock);
                    NodeParam commentParam = new NodeParam(comment1);
                    if (!Options.IsEV3Desktop)
                    {
                        const string paramname = "Color";
                        NodeParam valuep = new NodeParam(commentNode.XPath2SelectOne(@"string(*:p.CommentColor/text())").ToString());
                        //BlockConverter.Convert(lbComment.Block, paramname, argvaluep, false);
                        string color1 = "grey";
                        switch (valuep.Value)
                        {
                            case "Green":
                                color1 = "#519445"; break;
                            case "Orange":
                                color1 = "#DE891E"; break;
                            case "Purple":
                                color1 = "#7E22C1"; break;
                        }
                        valuep.ValueDetails = new { Color = color1 };
                        valuep.ValueType = NodeParam.ParamValueTypes.Color;
                        lbComment.Params.Add(paramname, valuep);

                        // set comment param value details (color)
                        commentParam.ValueDetails = valuep.ValueDetails;
                    }
                    lbComment.Params.Add("Comment", commentParam);
                    //int indexOfCurrent = lbComment.Parent.Children.IndexOf(lbCurrent);
                    //lbCurrentOutput.Add(lbComment);

                    var insertCommentTo = lbCurrent.Parent.Children;
                    int idx = insertCommentTo.IndexOf(lbCurrent);

                    //-- Comment cannot precede startblock - move it after
                    if (lbCurrent.Block == Node.BLOCK_StartBlock)
                    {
                        if (lbCurrent.Children?.Count > 0)
                        {
                            insertCommentTo = lbCurrent.Children;
                            idx = 1;
                        }
                        else
                        {
                            idx = Math.Min(idx + 1, insertCommentTo.Count - 1);
                        }
                    }

                    //-- add free comment to the node tree
                    if (idx >= 0) insertCommentTo.Insert(idx, lbComment);
                    else insertCommentTo.Add(lbComment);
                }
            }
        }
    }
}
