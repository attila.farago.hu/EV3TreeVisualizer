﻿using EV3ModelLib;
using EV3TreeVisLib.Logic;
using EV3TreeVisLib.Logic.Converter;
using EV3TreeVisLib.Printing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Wmhelp.XPath2; // project Lightweight XPath2 for .NET https://xpath2.codeplex.com/

namespace EV3TreeVisLib
{
    /// <summary>
    /// Class to traverse one ev3p file and create a tree of nodes representation
    /// </summary>
    internal partial class Ev3FileTraverser
    {
        /// <summary>
        /// Parameters to skip from processing
        /// </summary>
        static readonly string[] Int32ParamsToSkip = { "LoopIterationCount", "InterruptsToListenFor_16B03592_CD76_4D58_8DC3_E3C3091E327A", "InterruptValue" };

        /// <summary>
        /// Node Selector patterns to select blocks within a program
        /// </summary>
        internal const string NodeSelectorPatternBase = "*:ConfigurableMethodCall|*:ConfigurableWaitFor|*:ConfigurableWhileLoop|*:PairedConfigurableMethodCall";
        internal const string NodeSelectorPattern = "(" + NodeSelectorPatternBase + ")";
        internal const string NodeSelectorPatternWithStartAndComments = "(" + NodeSelectorPatternBase + "|*:Comment|*:StartBlock" + ")";

        /// <summary>
        /// Data wire registry keeping track of wires
        /// </summary>
        private DataWireRegistry dataWireRegistry;

        /// <summary>
        /// Preliminary step - to explore myblock definitions and icons
        /// </summary>
        /// <param name="fileStream"></param>
        /// <param name="filename"></param>
        internal void TraverseAndAddMyBlockDefinition(Stream fileStream, string filename)
        {
            XDocument xdoc = XDocument.Load(fileStream);
            string icon = xdoc.XPath2SelectOne("string(//*:PolyGroup/@IconBaseName)").ToString();
            BlockNameConverter.AddDictMyBlockIcons(filename, icon + "_Diagram.png");
        }

        /// <summary>
        /// Options for File Traverser
        /// </summary>
        internal class OptionsClass
        {
            internal bool HandleAndReportErrors { get; set; } = true;
            internal bool IsEV3Desktop { get; set; } = true;
        }
        private OptionsClass Options { get; set; }

        /// <summary>
        /// Main routine to execute file traversal and node generation
        /// </summary>
        /// <param name="fileStream"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        internal List<LogBlock> TraverseFile(Stream fileStream, string fileName, OptionsClass options)
        {
            if (options == null) options = new OptionsClass();
            this.Options = options;

            try
            {
                // initialize basic structures
                dataWireRegistry = new DataWireRegistry();
                XDocument xdoc = XDocument.Load(fileStream);

                // Add main parent block
                XElement rootNode = xdoc.XPath2SelectOne<XElement>("//*:BlockDiagram");
                LogBlock lbRootBlock = new LogBlock(rootNode)
                {
                    Block = Path.GetFileNameWithoutExtension(fileName),
                    BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(fileName), // check if we are a MYBLOCK (icon) 
                    Id = LogBlock.ExtraBlocks.ROOT_ID,
                    BlockType = LogBlock.BlockTypes.Root,
                    Parent = null
                };
                if (lbRootBlock.BlockDisplayDetails.BlockFamily == BlockNameConverter.CONST_BlockFamily_MyBlocks) lbRootBlock.IsMyBlock = true;
                List<LogBlock> lbLogBlocks = new List<LogBlock>() { lbRootBlock };

                // if this is a MYBLOCK - add incoming parameters to start node
                foreach (XElement megaNode in rootNode.XPath2SelectElements("*:ConfigurableMegaAccessor").ToList())
                {
                    ProcessNodeTerminals(megaNode, lbRootBlock, lbRootBlock.Scope + "." + lbRootBlock.Id, false, true); //force all input processing
                }

                // process StartBlocks
                List<LogBlock> lbOutput = lbRootBlock.Children = new List<LogBlock>() { };
                var startBlocks = rootNode.XPath2SelectElements("*:StartBlock").ToList();
                foreach (XElement startBlock in startBlocks)
                {
                    if (startBlock == null) return lbLogBlocks;

                    // start processing with the "StartBlock" block
                    LogBlock lbStartBlock = new LogBlock(startBlock) { Parent = lbRootBlock };
                    List<LogBlock> sb = new List<LogBlock>();

                    if (startBlocks.Count == 1) // only one startblocks
                    {
                        // add startblock
                        sb.Add(lbStartBlock);
                    }

                    // process node data
                    List<LogBlock> sb2 = ProcessNode(startBlock, rootNode, lbStartBlock, startBlocks.Count == 1);
                    sb.AddRange(sb2);
                    CheckAndAddEmptyNode(lbStartBlock, sb);

                    if (startBlocks.Count > 1) // more than one startblocks
                    {
                        lbOutput.Add(lbStartBlock);
                        lbStartBlock.Children = new List<LogBlock>(sb);
                    }
                    else // only one startblock
                    {
                        lbOutput.AddRange(sb);
                    }
                }

                // free standing comment boxes for all nodes
                var allnodesflat = lbRootBlock.ChildrenTraversed();
                PostProcessFreeStandingCommentBlocks(lbRootBlock, allnodesflat);

                // print debug information about datawires
                if (Output.Common.DebugFlag)
                    dataWireRegistry.Debug_PrintContentsToConsole();

                return lbLogBlocks;
            }
            catch (Exception e)
            {
                // if there is an exception - only fail the current file
                ConsoleExt.WriteLine($"Error processing file {fileName}", ConsoleColor.Gray);
                ConsoleExt.WriteLine(e.ToString(), ConsoleColor.Gray);

                if (!options.HandleAndReportErrors) throw; // re-throw is error handling is disabled
                return null;
            }
            finally
            {
                //arrFreeComments = null;
                dataWireRegistry = null;
            }
        }

        /// <summary>
        /// Process Node parameters, terminals
        /// </summary>
        /// <param name="thisnode"></param>
        /// <param name="logBlock"></param>
        /// <param name="scope"></param>
        private void ProcessNodeTerminals(XElement thisnode, LogBlock logBlock, string scope,
            bool allowSkipUnwiredOutputs = true, bool addParamnamesToDataWiresForRoot = false)
        {
            IEnumerable<XElement> terminals = thisnode.XPath2SelectElements(
                Options.IsEV3Desktop ? @"*:ConfigurableMethodTerminal/*:Terminal" : @"*:ConfigurableTerminal"
                ).ToList<XElement>();
            foreach (XElement item in terminals)
            {
                string sDirection = item.XPath2SelectOne("string(@Direction)").ToString();
                string argname = item.XPath2SelectOne("string(@Id)").ToString();
                string sDataType = item.XPath2SelectOne("string(@DataType)").ToString();
                string argvalue = item.XPath2SelectOne(
                    Options.IsEV3Desktop ? "string(../@ConfiguredValue)" : "string(@ConfiguredValue)"
                    ).ToString();
                bool bIncoming = sDirection == "Input"; // Otherwise it is assumed to be Output
                XAttribute wire = item.Attribute("Wire");

                NodeParam argvaluep = new NodeParam(argvalue) { IsInput = bIncoming };

                //TODO: argname based on name+node
                switch (argname)
                {
                    case AttributeConverter.PARAM_Port_Input:
                        argvaluep.ValueType = NodeParam.ParamValueTypes.SensorPort;
                        break;
                    case AttributeConverter.PARAM_MotorPort_Input:
                        argvaluep.ValueType = NodeParam.ParamValueTypes.MotorPort;
                        break;
                    case AttributeConverter.PARAM_MotorPorts_Input:
                        argvaluep.ValueType = NodeParam.ParamValueTypes.MotorPorts;
                        break;
                    case AttributeConverter.PARAM_Direction_Input:
                        argvaluep.ValueType = NodeParam.ParamValueTypes.WaitForChange;
                        break;
                    case AttributeConverter.PARAM_Comparison_Input:
                        argvaluep.ValueType = NodeParam.ParamValueTypes.Comparison;
                        break;
                    case AttributeConverter.PARAM_Comparison2_Input:
                        argvaluep.ValueType = NodeParam.ParamValueTypes.Comparison2;
                        break;
                        //TODO: Add/process other ParamTypes as well
                        /*
                         *             
                    Color = 0, Colors = 1, TouchState = 2, TouchStates = 3, LEDColor = 4, PlayType = 5, OutputBrickButtonID = 6, OutputBrickButtons = 7, EV3Ps = 8,
                    MotorPort = 9, MotorPorts = 10, SensorPort = 11, WaitForChange = 12, Comparison = 13, Comparison2 = 14,
                    IROutputButtonID = 15,
                    RGFbase64 = 255
        */
                }

                if (!(sDataType == "Int32" && Int32ParamsToSkip.Contains(argname)))
                {
                    if (sDirection == "Output" && wire == null && allowSkipUnwiredOutputs) continue; // skip output terminals with no data wires - irrelevant
                    if (wire != null)
                    {
                        string dw = dataWireRegistry.TranslateWireInScope(scope, wire.Value, logBlock, sDataType, bIncoming, true,
                            addParamnamesToDataWiresForRoot ? "[" + NodeParams.UnescapeAndShortenParamName(argname) + "]" : null);
                        argvaluep.Value = dw; // Wired: port has data wire
                        argvaluep.WireId = dw;

                        // if root and megablock @AccessorType=Output as it does not "exists" yet in sequence
                        if (logBlock.BlockType == LogBlock.BlockTypes.Root)
                        {
                            string sAccessortype = thisnode.XPath2SelectOne("string(@AccessorType)").ToString();
                            bool isDirectionOutput = sAccessortype == "Output";
                            if (isDirectionOutput) dataWireRegistry.AddIgnoredScopeWire(DataWireRegistry.GetFullName(scope, wire.Value));

                            //-- rewrite IsInput direction based on AccessorType
                            argvaluep.IsInput = !isDirectionOutput;
                        }
                    }
                    else
                    {
                        string targetid = BlockNameConverter.UnescapeAndTruncVIXExtension(thisnode.XPath2SelectOne("string(@Target)").ToString());
                        string argname1 = NodeParams.UnescapeAndShortenParamName(argname);
                        BlockConverter.Convert(targetid, argname1, argvaluep, true);
                    }

                    //// mathblock name conversion
                    //if (logBlock.Block.StartsWith("Math."))
                    //{
                    //    switch (argname)
                    //    {
                    //        case "X": argname = "A"; break;
                    //        case "Y": argname = "B"; break;
                    //    }
                    //}

                    logBlock.Params.Add(argname, argvaluep);
                }
            }
        }

        /// <summary>
        /// Process one node from xml to LogBlock
        /// </summary>
        /// <param name="thisnode"></param>
        /// <param name="rootNode"></param>
        /// <param name="lbCurrent"></param>
        /// <param name="bAddNewNodesToCurrentParent">New nodes added to current node or parent of current node</param>
        /// <returns></returns>
        private List<LogBlock> ProcessNode(XElement thisnode, XElement rootNode, LogBlock lbCurrent, bool bAddNewNodesToCurrentParent = true)
        {
            List<LogBlock> lbOutput = new List<LogBlock>();
            string Id = null;
            try
            {
                string nodetarget = thisnode.XPath2SelectOne("string(@Target)").ToString();
                if (string.IsNullOrEmpty(nodetarget)) nodetarget = thisnode.Name.LocalName;
                string sequencewire_out = thisnode.XPath2SelectOne(Options.IsEV3Desktop ? "string(*:Terminal[@Id='SequenceOut']/@Wire)" : "string(@Id)").ToString();
                // iPad uses sequencewire out for sequence input value

                lbCurrent.BlockType = nodetarget.EndsWith(".ev3p") ? LogBlock.BlockTypes.NativeEV3P : LogBlock.BlockTypes.Native;
                lbCurrent.Block = BlockNameConverter.Translate(nodetarget);
                lbCurrent.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(nodetarget);
                lbCurrent.Id = Id = thisnode.XPath2SelectOne("string(@Id)").ToString();

                // process node based on target
                switch (nodetarget)
                {
                    case BlockNameConverter.CONST_BlockTarget_CommentBlock:
                        {
                            // BLOCK: COMMENT block
                            string argname = "Comment";
                            string argvalue = thisnode.XPath2SelectOne("string(@" + argname + ")").ToString().Replace('\r', ' ').Replace('\n', ' ');
                            lbCurrent.Params.Add(argname, new NodeParam(argvalue));
                        }
                        break;
                    case BlockNameConverter.CONST_BlockTarget_DataLogMaster:
                        {
                            // BLOCK: DATALOGGER block

                            // check and add datalogger internal nodes
                            foreach (XElement subnode in thisnode.XPath2SelectElements("*:ConfigurableMethodCall"))
                            {
                                LogBlock lbCurrentSub = new LogBlock(subnode) { Parent = lbCurrent };

                                ProcessNode(subnode, thisnode, lbCurrentSub);
                                if (!string.IsNullOrEmpty(lbCurrentSub.Block))
                                {
                                    if (lbCurrent.Children == null) lbCurrent.Children = new List<LogBlock>();
                                    lbCurrent.Children.Add(lbCurrentSub);
                                }
                            }
                        }
                        break;
                    case BlockNameConverter.CONST_BlockTarget_ConfigurableWhileLoop:
                        {
                            // BLOCK: LOOP block
                            var sInterruptName = thisnode.XPath2SelectOne("string(@InterruptName)").ToString();
                            var sDiagramId = thisnode.XPath2SelectOne("string(@DiagramId)").ToString();
                            if (!string.IsNullOrEmpty(sInterruptName))
                                lbCurrent.Params.Add(Node.PARAM_LOOP_InterruptName, new NodeParam(sInterruptName));

                            // process tunnels for wire translation
                            // <ConfigurableWhileLoop.ConfigurableLoopTunnel AutoIndex="False" Id="b13" Terminals="n2=w14, d0=w11" Bounds="117 143 30 63" />
                            Dictionary<string, Tuple<string, string>> aTunnel2Datawire = dataWireRegistry.ProcessTunnels(lbCurrent, thisnode, false, "*:ConfigurableWhileLoop.ConfigurableLoopTunnel");

                            // next node wire
                            string wireout2 = null;

                            // process stop condition, loop index
                            // iPad: BuiltInMethodLoopIndex = "63" BuiltInMethodStopCondition = "64"
                            IEnumerable<XElement> aBuiltInMethods = thisnode.XPath2SelectElements(
                                Options.IsEV3Desktop ?
                                "*:ConfigurableWhileLoop.BuiltInMethod/*:ConfigurableMethodCall" :
                                "*:BuiltInConfigurableMethodCallLoopIndex|*:BuiltInConfigurableMethodCallStopCondition"
                                ).ToList();
                            foreach (XElement aBuildInMethod in aBuiltInMethods)
                            {
                                string sCallType = Options.IsEV3Desktop ?
                                    aBuildInMethod.XPath2SelectOne("string(../@CallType)").ToString() :
                                    aBuildInMethod.Name.LocalName.Replace("BuiltInConfigurableMethodCall", string.Empty);
                                string sTarget = aBuildInMethod.XPath2SelectOne("string(@Target)").ToString();

                                if (sCallType == "StopCondition")
                                {
                                    lbCurrent.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(sTarget);
                                    // update Switch block family coloring - not to update the sensor block
                                    lbCurrent.BlockDisplayDetails.BlockFamily = BlockNameConverter.CONST_BlockFamily_FlowControl;
                                    lbCurrent.Params.Add(Node.PARAM_LOOP_StopCondition, new NodeParam(BlockNameConverter.Translate(sTarget)));
                                }
                                else if (sCallType == "LoopIndex")
                                {
                                    if (Options.IsEV3Desktop)
                                    {
                                        // consider LoopIndex - as it can be an outgoing wire
                                        var aOutputDataWire = aBuildInMethod.XPath2SelectOne("string(*:Terminal[@Direction='Output']/@Wire)").ToString();
                                        // if loop index is not not wired, skip it as it is not relevant
                                        if (string.IsNullOrEmpty(aOutputDataWire)) continue;
                                    }
                                    else
                                    {
                                        // iPad: for ipad LoopIndex contains Id for children SequenceId node
                                        wireout2 = aBuildInMethod.Attribute("Id").Value;
                                    }
                                }

                                // note: scope must translated to be "within" the loop node
                                ProcessNodeTerminals(aBuildInMethod, lbCurrent, lbCurrent.FullId);
                            }

                            // Desktop: check while loop internal nodes - LoopIndex and StopCondition is ConfigurableWhileLoop.BuiltInMethod/ConfigurableMethodCall
                            if (Options.IsEV3Desktop)
                                wireout2 = thisnode.XPath2SelectOne("string(*:ConfigurableWhileLoop.BuiltInMethod/*:ConfigurableMethodCall/*:Terminal[@Id='SequenceOut']/@Wire)").ToString();
                            List<LogBlock> sb2 = new List<LogBlock>();
                            if (!string.IsNullOrEmpty(wireout2))
                                sb2 = GetNextNode(wireout2, thisnode, lbCurrent);

                            // Collect and Add tunnel shortcut nodes
                            dataWireRegistry.CollectAndAddTunnelShortcutNodes(thisnode, lbCurrent, aTunnel2Datawire, sb2);

                            // still add an "empty" block if there are no blocks in case
                            CheckAndAddEmptyNode(lbCurrent, sb2);

                            // weave all children under the loop node
                            if (sb2.Count > 0)
                            {
                                if (lbCurrent.Children == null) lbCurrent.Children = new List<LogBlock>();
                                lbCurrent.Children.AddRange(sb2);
                            }
                        }
                        break;
                    case string s when (thisnode.Name.LocalName == BlockNameConverter.CONST_BlockName_Switch_PairedConfigurableMethodCall):
                        {
                            // BLOCK: SWITCH block
                            //   switch block starts with PairedConfigurableMethodCall
                            //   continues with matching ConfigurableFlatCaseStructure/ConfigurableCaseStructure
                            string pairedId = thisnode.XPath2SelectOne("string(@PairedStructure)").ToString();
                            XElement pairedNode = rootNode.XPath2SelectOne("(*:ConfigurableFlatCaseStructure|*:ConfigurableCaseStructure)[@Id='" + pairedId + "']") as XElement;
                            string defaultPatternId = pairedNode.Attribute("Default").Value;
                            List<XElement> caseNodes = pairedNode.XPath2SelectElements("*:ConfigurableFlatCaseStructure.Case|*:ConfigurableCaseStructure.Case").ToList();
                            lbCurrent.BlockType = LogBlock.BlockTypes.NativeSwitch;

                            string nodetarget1 = BlockNameConverter.UnescapeAndTruncVIXExtension(nodetarget);
                            if (caseNodes.Count > 0)
                            {
                                var casePatterns = (from caseNode in caseNodes.Cast<XElement>()
                                                    let caseId = caseNode.Attribute("Id").Value
                                                    let casePattern = caseNode.Attribute("Pattern")?.Value ?? String.Empty
                                                    select casePattern)
                                                   .ToArray();
                                NodeParam casePatternsParam = new NodeParam("[" + string.Join(",", casePatterns) + "]");
                                // convert casepatterns with block converters
                                string outputTerminal = thisnode.XPath2SelectOne<XAttribute>(
                                    Options.IsEV3Desktop ?
                                        "*:ConfigurableMethodTerminal/*:Terminal[@Direction='Output']/@Id" :
                                        "*:ConfigurableTerminal[@Direction='Output' and @Id!='Result']/@Id"
                                    ).Value.ToString();
                                var casePatternsValue = BlockConverter.Convert(nodetarget1, outputTerminal, casePatternsParam, true);
                                lbCurrent.Params.Add(Node.PARAM_SWITCH_Cases, casePatternsParam);
                            }
                            ProcessNodeTerminals(thisnode, lbCurrent, lbCurrent.Scope);

                            // override block display settings
                            lbCurrent.Block = Node.BLOCK_SwitchPrefix + lbCurrent.Block;
                            lbCurrent.BlockDisplayDetails.BlockFamily = BlockNameConverter.CONST_BlockFamily_FlowControl;

                            // process tunnels
                            //<ConfigurableCaseStructure.ConfigurableCaseTunnel Id="b40" Terminals="n37=w28, D38=w49, D39=w15" Bounds="144 1166 30 63" />
                            Dictionary<string, Tuple<string, string>> aTunnel2Datawire = dataWireRegistry.ProcessTunnels(lbCurrent, pairedNode, true, "*:ConfigurableCaseStructure.ConfigurableCaseTunnel");

                            // iterate through cases
                            lbCurrent.Children = new List<LogBlock>();
                            foreach (XElement caseNode in caseNodes)
                            {
                                string caseId = caseNode.Attribute("Id").Value;
                                NodeParam casePattern = new NodeParam(caseNode.Attribute("Pattern")?.Value ?? String.Empty);
                                var wireout2 = caseNode.XPath2SelectOne(
                                                    Options.IsEV3Desktop ?
                                                        @"string(*:SequenceNode/*:Terminal[@Direction='Output']/@Wire)" :
                                                        @"string(*:SequenceNode[*:Terminal[@Direction='Output']]/@Id)"
                                                    ).ToString();

                                // Add switch fork node
                                LogBlock lbSwitchForkNode = new LogBlock(caseNode)
                                {
                                    Block = LogBlock.ExtraBlocks.CASEITEM,
                                    BlockType = LogBlock.BlockTypes.CaseItem,
                                    Parent = lbCurrent,
                                    Id = caseId,
                                    BlockDisplayDetails = new BlockNameConverter.LegoBlockRecord() { BlockFamily = BlockNameConverter.CONST_BlockFamily_FlowControl }
                                };
                                BlockConverter.Convert(nodetarget1, null, casePattern, false);
                                casePattern.Value += caseId == defaultPatternId ? "*" : string.Empty;
                                lbSwitchForkNode.Params.Add(Node.PARAM_SWITCH_Pattern, casePattern);
                                lbCurrent.Children.Add(lbSwitchForkNode);

                                // add nodes succeeding the switch fork node
                                List<LogBlock> sb2 = GetNextNode(wireout2, caseNode, lbSwitchForkNode);

                                // Collect and Add tunnel shortcut nodes
                                dataWireRegistry.CollectAndAddTunnelShortcutNodes(caseNode, lbSwitchForkNode, aTunnel2Datawire, sb2);

                                // still add an "empty" block if there are no blocks in case
                                CheckAndAddEmptyNode(lbSwitchForkNode, sb2);

                                // weave all children under the switch node
                                if (sb2.Count > 0)
                                    lbSwitchForkNode.Children = sb2;
                            }
                            // watch out: next SequenceNode is read from ConfigurableFlatCaseStructure/ConfigurableCaseStructure
                            sequencewire_out = pairedNode.XPath2SelectOne(Options.IsEV3Desktop ?
                                                                                "string(*:Terminal[@Id='SequenceOut']/@Wire)" :
                                                                                "string(@Id)"
                                                                                ).ToString();
                        }
                        break;
                    default:
                        {
                            // BLOCK: others - process Terminal nodes (parameters)
                            ProcessNodeTerminals(thisnode, lbCurrent, lbCurrent.Scope);

                            // in case of a wait block - change blockfamily and preprend name with Wait.
                            if (thisnode.Name.LocalName == BlockNameConverter.CONST_BlockName_ConfigurableWaitFor)
                            {
                                // override block display settings                              
                                lbCurrent.BlockType = LogBlock.BlockTypes.NativeWait;
                                lbCurrent.Block = Node.BLOCK_WaitForPrefix + lbCurrent.Block;
                                lbCurrent.BlockDisplayDetails.BlockFamily = BlockNameConverter.CONST_BlockFamily_FlowControl;
                            }
                        }
                        break;
                }

                // if there is a next node in sequence wire chain - process it
                if (!string.IsNullOrEmpty(sequencewire_out))
                {
                    List<LogBlock> sb = GetNextNode(sequencewire_out, rootNode, bAddNewNodesToCurrentParent ? lbCurrent.Parent : lbCurrent);
                    if (sb.Count > 0) lbOutput.AddRange(sb);
                }
            }
            catch (Exception e)
            {
                // if there is an exception - only fail the current node
                ConsoleExt.WriteLine($"Error processing node {Id}", ConsoleColor.Gray);
                ConsoleExt.WriteLine(e.ToString(), ConsoleColor.Gray);

                if (!Options.HandleAndReportErrors) throw; // re-throw is error handling is disabled
            }

            return lbOutput;
        }


        /// <summary>
        /// Check and Add empty node to a parent
        /// </summary>
        /// <param name="lbParent"></param>
        /// <param name="lbChildrenList"></param>
        private static void CheckAndAddEmptyNode(LogBlock lbParent, List<LogBlock> lbChildrenList)
        {
            if (lbChildrenList.Where(elem => elem != lbParent).Count() == 0)
            {
                lbChildrenList.Add(new LogBlock(null)
                {
                    Block = LogBlock.ExtraBlocks.EMPTY,
                    BlockType = LogBlock.BlockTypes.Empty,
                    BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(LogBlock.ExtraBlocks.EMPTY),
                    Parent = lbParent
                });
            }
        }

        /// <summary>
        /// Iterate to next node in the sequence wire chain
        /// </summary>
        /// <param name="wirein"></param>
        /// <param name="rootNode"></param>
        /// <param name="lbParent"></param>
        /// <returns></returns>
        private List<LogBlock> GetNextNode(string wirein, XElement rootNode, LogBlock lbParent)
        {
            List<LogBlock> lbOutput = new List<LogBlock>();
            IList nextNodes = null;
            if (Options.IsEV3Desktop)
            {
                nextNodes = rootNode.XPath2SelectElements(Ev3FileTraverser.NodeSelectorPattern +
                    "/*:Terminal[@Id='SequenceIn' and @Wire='" + wirein + "']/..").ToList();
            }
            else
            {
                // in EV3Ipad version wires contain information for next nodes
                var sequenceJoins = rootNode.XPath2SelectValues(@"*:Wire[matches(@Joints,'^N\(" + wirein + @"\:(?:SequenceOut|Output)\)|^N\(\d+:" + wirein + @"\)')]/@Joints").Select(e => e.ToString()).ToList();
                var joinNodes = new List<XElement>();
                foreach (string sequenceJoin in sequenceJoins)
                {
                    // multiple N(144:SequenceIn) nodes can direct to the next node
                    // loops get different format - N(59:57) where node:57, SequenceBorderNode Id:59
                    MatchCollection regexmatches = new Regex(@"(?<=N\(.+\).* )((N\((?<node>\d+):SequenceIn\))|(N\(\d+:(?<node>\d+)\)))").Matches(sequenceJoin);
                    foreach (Match regexmatch in regexmatches)
                    {

                        var targetid = regexmatch.Groups[4].Value;
                        XElement elem = rootNode.XPath2SelectElement(Ev3FileTraverser.NodeSelectorPattern + "[@Id='" + targetid + "']");
                        if (elem != null) joinNodes.Add(elem);
                    }
                }
                nextNodes = joinNodes;
            }

            // iterate through each node where the incoming wire is connected to
            var parallelCounter = 0;
            LogBlock lbForkParent = null;
            foreach (XElement thisnode in nextNodes)
            {
                LogBlock lbCurrent = new LogBlock(thisnode)
                {
                    WireIn = wirein,
                    Parent = lbParent
                };

                List<LogBlock> lbCurrentOutput = lbOutput;
                // if there are more parallel threads look we need a fork parent node and children
                parallelCounter++;
                if (nextNodes.Count > 1)
                {
                    // if fork parent is not initialized, create it for the first time
                    if (lbForkParent == null)
                    {
                        lbForkParent = new LogBlock(null)
                        {
                            Block = LogBlock.ExtraBlocks.FORK,
                            BlockType = LogBlock.BlockTypes.ForkParent,
                            Children = new List<LogBlock>(),
                            WireIn = wirein,
                            Parent = lbParent
                        };
                        lbForkParent.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(lbForkParent.Block);
                        lbForkParent.Params.Add("branches", new NodeParam(nextNodes.Count.ToString()));
                        lbOutput.Add(lbForkParent);
                        lbCurrent.WireIn = null;
                    }

                    // FORK create subitem to add chain to
                    LogBlock lbForkItem = new LogBlock(null)
                    {
                        Block = LogBlock.ExtraBlocks.FORKITEM,
                        BlockType = LogBlock.BlockTypes.ForkItem,
                        Children = new List<LogBlock>() { },
                        Parent = lbForkParent,
                    };
                    lbForkItem.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(lbForkItem.Block);
                    //lbForkItem.Params.Add("#", new NodeParam(parallelCounter.ToString()));
                    lbCurrent.Parent = lbForkItem;
                    lbForkParent.Children.Add(lbForkItem);

                    lbCurrentOutput = lbForkItem.Children;
                }

                // chain the current node into the main chain
                lbCurrentOutput.Add(lbCurrent);

                List<LogBlock> sb = ProcessNode(thisnode, rootNode, lbCurrent);
                if (sb.Count > 0) lbCurrentOutput.AddRange(sb);

            }

            // reorder ForkItems based on their size (total number of nodes) ascending order (show short forks first)
            if (lbForkParent != null)
            {
                var orderedForkItems = lbForkParent.Children
                    .OrderBy(elem => elem.ChildrenCount(true))
                    .ToList();
                orderedForkItems
                    .Select((elem, index) =>
                    {
                        elem.Params.Add("#", new NodeParam((index + 1).ToString()));
                        return elem;
                    })
                    .ToList();
                lbForkParent.Children = orderedForkItems.ToList();
            }

            return lbOutput;
        }
    }
}
