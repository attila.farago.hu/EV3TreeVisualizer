﻿using EV3TreeVisLib.Logic;
using EV3TreeVisLib.Logic.Converter;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Wmhelp.XPath2;

namespace EV3TreeVisLib
{
    /// <summary>
    /// Class to process generic EV3 project related statistics and build project tree
    /// </summary>
    public class Ev3Project
    {
        /// <summary>
        /// Project name
        /// </summary>
        public string Name { get; private set; }

        internal Dictionary<string, Tuple<string, List<string>>> GlobalVariables { get; } = new Dictionary<string, Tuple<string, List<string>>>();
        internal Dictionary<string, List<string>> MediaFiles { get; } = new Dictionary<string, List<string>>();
        internal Dictionary<string, string> ProjectInfo { get; } = new Dictionary<string, string>();
        internal Dictionary<string, List<string>> Dependencies_UsedBy = new Dictionary<string, List<string>>();
        internal Dictionary<string, string> Warnings = new Dictionary<string, string>();

        /// <summary>
        /// graphic file extensions
        /// </summary>
        private static readonly List<string> GRAPHICFILESEXT = new List<string>()
        {
           ".rgf", ".rsf",
        };

        /// <summary>
        /// Process the project file and program files and create a tree of dependencies
        /// </summary>
        internal LogBlock ProcessProject(ZipFile zf, string zipName)
        {
            Name = zipName;

            ZipEntry ze = zf.GetEntry("Project.lvprojx");
            if (ze == null) return null;
            Stream projectFileStream = zf.GetInputStream(ze);

            LogBlock lbRoot = new LogBlock(null)
            {
                Block = zipName,
                Children = new List<LogBlock>(),
                BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(zipName),
                BlockType = LogBlock.BlockTypes.ProjectRoot
            };

            //--------------------------
            // explore global variables
            XDocument xdocProject = XDocument.Load(projectFileStream);
            foreach (XElement elem in xdocProject.XPath2SelectElements("//*:NamedGlobalData/*:Datum"))
            {
                var vname = elem.Attribute("Name").Value.ToString();
                var vtype = elem.Attribute("Type").Value.ToString();
                var vtype2 = ProjectVariableDataType_To_ProgramDataType(vtype);
                GlobalVariables[vname] = new Tuple<string, List<string>>(vtype2, new List<string>());
            }

            //--------------------------
            // explore media files
            foreach (XElement elem in xdocProject.XPath2SelectElements("//*:DefinitionReference[@DocumentTypeIdentifier='NationalInstruments.ExternalFileSupport.Modeling.ExternalFileType']"))
            {
                var name = elem.Attribute("Name").Value.ToString();
                var name1 = BlockNameConverter.UnescapeBlockName(name);

                if (GRAPHICFILESEXT.Contains(Path.GetExtension(name1)))
                {
                    MediaFiles.Add(name1, new List<string>());
                }
            }

            //--------------------------
            // Explore dependencies and global variables usage
            Dictionary<string, List<string>> dependencies = new Dictionary<string, List<string>>();
            Dictionary<string, LogBlock> dependencies2 = new Dictionary<string, LogBlock>();
            foreach (XElement elem in xdocProject
                .XPath2SelectElements("//*:SourceFileReference")
                .OrderBy(elem => elem.Attribute("Name").Value.ToString())
                .ToList())
            {
                var nameattr = elem.Attribute("Name").Value.ToString();
                var blockname = BlockNameConverter.UnescapeBlockName(nameattr);
                if (Path.GetExtension(blockname) == ".ev3p")
                {
                    //var name2 = Path.GetFileNameWithoutExtension(name1);
                    if (!dependencies.ContainsKey(blockname)) dependencies.Add(blockname, new List<string>());
                    var programNode = new LogBlock(null)
                    {
                        Block = Path.GetFileNameWithoutExtension(blockname),
                        BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(blockname),
                        BlockType = LogBlock.BlockTypes.NativeEV3P,
                        Children = new List<LogBlock>()
                    };
                    lbRoot.Children.Add(programNode);
                    dependencies2[blockname] = programNode;

                    Stream programFileStream = zf.GetInputStream(zf.GetEntry(blockname));
                    XDocument xdocProgram = XDocument.Load(programFileStream);

                    // ev3p files calling in the program
                    xdocProgram.XPath2SelectValues("//*:ConfigurableMethodCall[@Target and contains(@Target,'.ev3p')]/@Target")
                                    .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                                    .Select(blockname2 => BlockNameConverter.UnescapeBlockName(blockname2.Value))
                                    .Distinct()
                                    .ToList().ForEach(blockname2a =>
                                        {
                                            //if (!Dependencies.ContainsKey(item2)) Dependencies.Add(item2, new List<string>());
                                            dependencies[blockname].Add(blockname2a);

                                            if (!Dependencies_UsedBy.ContainsKey(blockname2a)) Dependencies_UsedBy.Add(blockname2a, new List<string>());
                                            Dependencies_UsedBy[blockname2a].Add(blockname);
                                        });

                    // variables useage in the ev3p program
                    xdocProgram.XPath2SelectValues("//*:ConfigurableMethodCall[contains(@Target,'Global')]/*:ConfigurableMethodTerminal/*:Terminal[@Id='name']/../@ConfiguredValue")
                        .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                        .Select(gvarname => gvarname.Value)
                        .Distinct()
                        .ToList().ForEach(gvarname1 =>
                        {
                            if (!string.IsNullOrEmpty(gvarname1))
                            {
                                if (!GlobalVariables.ContainsKey(gvarname1))
                                {
                                    // retrieve the data type from the program
                                    var vtype2 = xdocProgram.XPath2SelectOne<XAttribute>("//*:ConfigurableMethodCall[contains(@Target,'Global')]/*:ConfigurableMethodTerminal[@ConfiguredValue='" + gvarname1 + "']/..//*:ConfigurableMethodTerminal/*:Terminal[@Id='valueIn' or @Id='valueOut']/@DataType").Value;
                                    GlobalVariables.Add(gvarname1, new Tuple<string, List<string>>(vtype2, new List<string>()));

                                    AddWarning($"Project missing variable {gvarname1}[{DataWireRegistry.TranslateWireDataTypeToDisplay(vtype2)}]", blockname);
                                }
                                GlobalVariables[gvarname1].Item2.Add(blockname);
                            }
                        });

                    // get all nodes count
                    var totalnodes = xdocProgram.XPath2SelectOne("count(" + "//" + Ev3FileTraverser.NodeSelectorPatternWithStartAndComments + ")").ToString();
                    programNode.Params.AddString("#", totalnodes);

                    // mediafiles usage / usedby 
                    xdocProgram.XPath2SelectValues(@"//*:ConfigurableMethodCall[@Target='PlaySoundFile\.vix']/*:ConfigurableMethodTerminal/*:Terminal[@Id='Name']/../@ConfiguredValue")
                        .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                        .Select(mediafile => mediafile.Value + ".rsf")
                        .Union(
                            xdocProgram.XPath2SelectValues(@"//*:ConfigurableMethodCall[@Target='DisplayFile\.vix']/*:ConfigurableMethodTerminal/*:Terminal[@Id='Filename']/../@ConfiguredValue")
                            .OfType<Wmhelp.XPath2.Value.UntypedAtomic>()
                            .Select(mediafile => mediafile.Value + ".rgf")
                        )
                        .Distinct()
                        .ToList().ForEach(mediafile1 =>
                        {
                            if (MediaFiles.ContainsKey(mediafile1))
                            {
                                MediaFiles[mediafile1].Add(Path.GetFileNameWithoutExtension(blockname));
                            }
                            else
                            {
                                // Warning: a called media is missing
                                string mediaType = GetMediaTypeByExtension(mediafile1);
                                AddWarning($"Project missing {mediaType} '{mediafile1}'", blockname);
                            }
                        });
                }
            }

            // check for unused variables and media files
            foreach (var gv in GlobalVariables)
            {
                if (!gv.Value.Item2.Any())
                {
                    AddWarning($"Unused variable {gv.Key}[{DataWireRegistry.TranslateWireDataTypeToDisplay(gv.Value.Item1)}]", null);
                }
            }
            foreach (var mf in MediaFiles)
            {
                if (!mf.Value.Any())
                {
                    string mediaFile = Path.GetFileNameWithoutExtension(mf.Key);
                    string mediaType = GetMediaTypeByExtension(mf.Key);
                    AddWarning($"Unused media {mediaType} file {mediaFile}", null);
                }
            }

            // post process dependencies with LogBlocks
            foreach (var kvp in dependencies)
            {
                var block1 = kvp.Key;
                var called_blocks = kvp.Value;
                var lb1 = dependencies2[block1];
                foreach (var block2 in called_blocks)
                {
                    if (dependencies2.ContainsKey(block2))
                    {
                        var lb2 = dependencies2[block2];
                        if (!lb1.Children.Contains(lb2))
                        {
                            lb1.Children.Add(lb2);
                            // remove from top if used anywhere else
                            lbRoot.Children.Remove(lb2);
                        }
                    }
                    else
                    {
                        // Warning: a called MyBlock is missing
                        AddWarning($"Project missing myblock {block2}", block1);
                    }
                }
                // optional: add variables 
                var globalVariablesUsed = GlobalVariables
                                          .Where(item => item.Value.Item2.Contains(block1))
                                          .OrderBy(gvarkvp => gvarkvp.Key)
                                          .Select(gvarkvp => $"{gvarkvp.Key}[{DataWireRegistry.TranslateWireDataTypeToDisplay(gvarkvp.Value.Item1)}]")
                                          .ToList();
                if (globalVariablesUsed.Count > 0) lb1.Params.AddString("Vars", string.Join(", ", globalVariablesUsed));
            }
            // order callee graph by alphanumeric
            foreach (var kvpDeps in dependencies2)
            {
                var lb1 = kvpDeps.Value;
                lb1.Children.OrderBy(lbi => lbi.Block).ToList();
            }

            //--------------------------
            // get project infos
            foreach (var filename in new string[] { "___CopyrightYear", "___ProjectTitle", "___ProjectDescription" })
            {
                ZipEntry ze1 = zf.GetEntry(filename);
                if (ze1 == null) continue;
                using (StreamReader reader = new StreamReader(zf.GetInputStream(ze1), Encoding.UTF8))
                {
                    var name1 = filename.Replace("___", "");
                    var value = reader.ReadToEnd().Trim().Replace("\r", "\n");
                    ProjectInfo.Add(name1, value);
                    lbRoot.Params.AddString(name1, value);
                }
            }

            //--------------------------
            // add mediafiles to lbRoot
            foreach (var extIter in GRAPHICFILESEXT)
            {
                var media = MediaFiles
                                .Where(mediakvp => Path.GetExtension(mediakvp.Key) == extIter)
                                .OrderBy(mediakvp => mediakvp.Key)
                                .ToList();
                if (media.Count > 0)
                {
                    LogBlock lbMediaNode = new LogBlock(null)
                    {
                        Block = extIter == ".rgf" ? LogBlock.ExtraBlocks.PROJECT_IMAGES : LogBlock.ExtraBlocks.PROJECT_SOUNDS,
                        BlockType = extIter == ".rgf" ? LogBlock.BlockTypes.ProjectExtraImages : LogBlock.BlockTypes.ProjectExtraSounds
                    };
                    lbMediaNode.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(lbMediaNode.Block);

                    lbMediaNode.Children = new List<LogBlock>(
                        media.Select(mediakvp =>
                        {
                            var lb1 = new LogBlock(null)
                            {
                                Block = Path.GetFileNameWithoutExtension(mediakvp.Key),
                                BlockType = lbMediaNode.BlockType
                            };
                            lb1.BlockDisplayDetails = lbMediaNode.BlockDisplayDetails;
                            if (mediakvp.Value.Count > 0)
                                lb1.Params.Add(LogBlock.CONST_ATTR_USEDBY,
                                    new NodeParam(
                                        string.Join(" | ",
                                        mediakvp.Value.ToArray()
                                        //item.Value.Select(item2 => Path.GetFileNameWithoutExtension(item2)).ToArray()
                                        ))
                                    {
                                        ValueDetails = new { EV3Ps = mediakvp.Value.ToArray() },
                                        ValueType = NodeParam.ParamValueTypes.EV3Ps
                                    }
                                    );

                            //-- add rgf bitmap in base64
                            if (Path.GetExtension(mediakvp.Key) == ".rgf")
                            {
                                try
                                {
                                    ZipEntry ze1 = zf.GetEntry(mediakvp.Key);
                                    Stream rgfFileStream = zf.GetInputStream(ze1);
                                    var rgfbitmap = new Helpers.RGFHelper.RGFBitmap(rgfFileStream, ze1.Size);
                                    var bmp = rgfbitmap.ConvertToBitmap();
                                    var str = "data:image/bmp;base64,";
                                    var converter = new System.Drawing.ImageConverter();
                                    var bytes = (byte[])converter.ConvertTo(bmp, typeof(byte[]));
                                    str += System.Convert.ToBase64String(bytes);
                                    var param = new NodeParam(str);
                                    param.ValueType = NodeParam.ParamValueTypes.RGFbase64;
                                    lb1.Params.Add("bitmap", param);
                                }
                                catch
                                {
                                    //NOOP
                                }
                            }

                            return lb1;
                        }).ToList()
                    );
                    lbRoot.Children.Add(lbMediaNode);
                };
            }

            //--------------------------
            // add variables to lbRoot
            if (GlobalVariables.Any())
            {
                var lbVariables = new LogBlock(null)
                {
                    Block = LogBlock.ExtraBlocks.PROJECT_VARS,
                    BlockType = LogBlock.BlockTypes.ProjectExtraVariables
                };
                lbVariables.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(lbVariables.Block);

                lbVariables.Children = new List<LogBlock>(
                    GlobalVariables.Select(gvar =>
                              {
                                  var lb1 = new LogBlock(null) { Block = gvar.Key, BlockType = lbVariables.BlockType };
                                  lb1.Params.AddString("Type", gvar.Value.Item1);
                                  var usedby = gvar.Value.Item2.Select(elem => Path.GetFileNameWithoutExtension(elem));
                                  if (usedby.Any())
                                  {
                                      lb1.Params.Add(LogBlock.CONST_ATTR_USEDBY,
                                          new NodeParam(string.Join(" | ", usedby.ToArray()))
                                          {
                                              ValueType = NodeParam.ParamValueTypes.EV3Ps,
                                              ValueDetails = new
                                              {
                                                  EV3Ps = usedby.ToArray()
                                              }
                                          }
                                          );
                                  }
                                  lb1.BlockDisplayDetails = lbVariables.BlockDisplayDetails;
                                  return lb1;
                              })
                            );
                lbRoot.Children.Add(lbVariables);
            }

            //--------------------------
            // add warnings to lbRoot
            if (Warnings.Any())
            {
                var lbWarnings = new LogBlock(null)
                {
                    Block = LogBlock.ExtraBlocks.PROJECT_WARNINGS,
                    BlockType = LogBlock.BlockTypes.ProjectExtraWarnings
                };
                lbWarnings.BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(lbWarnings.Block);

                lbWarnings.Children = new List<LogBlock>(
                    Warnings.Select(item =>
                    {
                        var lb1 = new LogBlock(null) { Block = item.Key, BlockType = lbWarnings.BlockType };
                        //lb1.Params.AddString("Type", gvar.Value.Item1);
                        if (item.Value != null)
                        {
                            lb1.Params.Add("in",
                                            new NodeParam(item.Value)
                                            {
                                                ValueType = NodeParam.ParamValueTypes.EV3Ps,
                                                ValueDetails = new
                                                {
                                                    EV3Ps = new string[] { item.Value }
                                                }
                                            }
                                        );
                        }
                        lb1.BlockDisplayDetails = lbWarnings.BlockDisplayDetails;
                        return lb1;
                    })
                );

                lbRoot.Children.Add(lbWarnings);
            }


            //=================================================================================
            // print
            //Console.WriteLine("GlobalVariables:");
            //GlobalVariables.OrderBy(item => item.Key).ToList().ForEach(item =>
            //    {
            //        Console.Write($"   {item.Key} [{DataWireRegistry.TranslateWireDataTypeToDisplay(item.Value.Item1)}] => ");
            //        Console.Write(string.Join(" | ", item.Value.Item2.ToList().Select(item1 => Path.GetFileNameWithoutExtension(item1)).ToArray()));
            //        Console.WriteLine();
            //    }
            //);
            //Console.WriteLine("MediaFiles:");
            //MediaFiles.OrderBy(item => item).ToList().ForEach(item => Console.WriteLine($"   {item}"));
            //Console.WriteLine("Programs and dependencies:");

            //Printer.Mode = Printer.Modes.Console;
            //LogBlock.PrintLogBlockToConsole(lbRoot, false);

            //dependencies.OrderBy(item => item.Key).ToList().ForEach(item =>
            //{
            //    Console.Write($"   {Path.GetFileNameWithoutExtension(item.Key)}");
            //    if (item.Value.Count > 0)
            //    {
            //        Console.Write(" => ");
            //        Console.Write(string.Join(" | ", item.Value.Select(item2 => Path.GetFileNameWithoutExtension(item2)).ToArray()));
            //    }
            //    Console.WriteLine();
            //});
            //Console.WriteLine("ProjectInfo:");
            //ProjectInfo.OrderBy(item => item.Key).ToList().ForEach(item =>
            //{
            //    Console.WriteLine($"   {item.Key} - {item.Value.Replace("\n", "\n     ")}");
            //});

            return lbRoot;
        }

        /// <summary>
        /// Sort elements alphabetically - with project modification mode
        /// </summary>
        /// <param name="xdoc"></param>
        internal Tuple<XDocument, ZipEntry> Update_SortEntriesAlphabetically(ZipFile zf)
        {
            ZipEntry ze = zf.GetEntry("Project.lvprojx");
            if (ze == null) return null;
            Stream projectFileStream = zf.GetInputStream(ze);
            XDocument xdoc = XDocument.Load(projectFileStream);

            //-- Sort elements alphabetically (Programs, MyBlocks, Media)
            var xproject = xdoc.XPath2SelectElement(@"*:SourceFile/*:Namespace[@Name='Default']/*:Project");
            var xtarget = xproject.XPath2SelectElement(@"*:Target[@DocumentTypeIdentifier='VIVMTarget']");
            var xProjectReferences = xtarget.XPath2SelectElements(@"*:ProjectReference").ToArray();
            var xSourceFileReferences = xtarget.XPath2SelectElements(@"*:SourceFileReference")
                .OrderBy(elem => elem.XPath2SelectOne("string(@StoragePath)").ToString()).ToArray();
            var xDefinitionReference = xtarget.XPath2SelectElements(@"*:DefinitionReference")
                .OrderBy(elem => elem.XPath2SelectOne("string(@Name)").ToString()).ToArray();
            var xOthers = xtarget.Elements()
                .Except(xProjectReferences).Except(xSourceFileReferences).Except(xDefinitionReference)
                .ToArray();
            xtarget.Elements().Remove();
            xtarget.Add(xProjectReferences);
            xtarget.Add(xSourceFileReferences);
            xtarget.Add(xDefinitionReference);
            xtarget.Add(xOthers);

            //-- Sort variables elements alphabetically
            var xProjectSettings = xproject.XPath2SelectElement(@"*:ProjectSettings");
            var xNamedGlobalData = xProjectSettings.XPath2SelectElement(@"*:NamedGlobalData");
            var xDatums = xNamedGlobalData.XPath2SelectElements(@"*:Datum");
            xNamedGlobalData.ReplaceNodes(
                   xDatums.OrderBy(elem => elem.XPath2SelectOne("string(@Name)").ToString()).ToArray()
                   );

            return new Tuple<XDocument, ZipEntry>(xdoc, ze);
        }

        /// <summary>
        /// Media type by path extension
        /// </summary>
        /// <param name="mediafile1"></param>
        /// <returns></returns>
        private static string GetMediaTypeByExtension(string mediafile1)
        {
            return Path.GetExtension(mediafile1) == ".rgf" ? "graphic" : "sound";
        }

        /// <summary>
        /// Data type mappings
        /// </summary>
        private static readonly Dictionary<string, string> ProjectVariableDataType_To_ProgramDataType_Mapping = new Dictionary<string, string>
        {
            ["X3String"] = "String",
            ["X3Numeric"] = "Single",
            ["X3NumericArray"] = "Single[]",
            ["X3Boolean"] = "Boolean",
            ["X3BooleanArray"] = "Boolean[]",
        };

        /// <summary>
        /// Map data type as .ev3p and .ev3/project naming differ
        /// </summary>
        /// <param name="projectDatatype"></param>
        /// <returns></returns>
        private static string ProjectVariableDataType_To_ProgramDataType(string projectDatatype)
        {
            if (ProjectVariableDataType_To_ProgramDataType_Mapping.ContainsKey(projectDatatype ?? string.Empty))
                return ProjectVariableDataType_To_ProgramDataType_Mapping[projectDatatype];
            return string.Empty;
        }

        /// <summary>
        /// Add a warning message
        /// </summary>
        /// <param name="message"></param>
        private void AddWarning(string message, string block)
        {
            //ConsoleExt.WriteLine("WARNING: " + message, ConsoleColor.Red);
            Warnings.Add(message, Path.GetFileNameWithoutExtension(block));
        }
    }
}
