﻿using EV3ModelLib;
using EV3TreeVisLib.Helpers;
using EV3TreeVisLib.Logic;
using EV3TreeVisLib.Printing;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace EV3TreeVisLib
{
    /// <summary>
    /// Output mode enumeration
    /// </summary>
    public enum OutputMode { Console, Text, HTML, JSON, DOT, None };

    /// <summary>
    /// Zip/EV3 file traverser to explore and process all -matching- ev3p program files
    /// </summary>
    public class Ev3ZipTraverser
    {
        /// <summary>
        /// Options for Zip Traverser
        /// </summary>
        public class OptionsClass
        {
            public string SelectedProgram { get; set; } = null;
            public OutputMode OutputMode { get; set; } = OutputMode.Console;
            public bool ListOnly { get; set; } = false;
            public bool IsTreeForGit { get; set; } = false;
            public bool HandleAndReportErrors { get; set; } = true;
            public bool JSONIndentedFormat { get; set; } = true;
            public bool ProjectOnly { get; set; } = false;
            public bool DebugFlag { get; set; } = false;
            public bool DisplayMetrics { get; set; } = false;
            public bool UpdateProjectSortABC { get; set; } = false;
            internal bool IsEV3Desktop { get; set; } = false;
            public bool GenerateExtraAttributes { get; set; } = true;
        }
        internal OptionsClass Options { get; set; }
        internal Ev3FileTraverser Ev3FileTraverser { get; set; }

        protected ZipFile m_zipFile = null;
        protected string m_zipName = null;
        protected Ev3Project m_ev3project = null;

        public (string retString, Dictionary<string, LogBlock> retBlocks, Ev3Project retProject) TraverseZip2(string zipName, OptionsClass options)
        {
            using (var fs = File.Open(zipName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return TraverseZip2(fs, zipName, options);
            }
        }
        public (string retString, Dictionary<string, Node> retNodes, Ev3Project retProject) TraverseZip3(Stream zipFileStream, string zipName, OptionsClass options)
        {
            //-- decode and generate (old style)
            (string retString, Dictionary<string, LogBlock> retBlocks, Ev3Project retProject) = TraverseZip2(zipFileStream, zipName, options);

            //-- convert to new Node format
            var retNodes = retBlocks.ToDictionary(elem => elem.Key, elem => NodeConversion.Convert(elem.Value));

            //-- generate string (new style)
            if (!PrintHelper.PrintOptions.SkipPrinting)
            {
                StringBuilder sbretval = new StringBuilder(); int idx = 0;
                retNodes.Values.ToList().ForEach(node1 =>
                {
                    if (idx++ > 0) sbretval.AppendLine(PrintHelper.Separator());
                    sbretval.Append(node1.GetPMDListString());
                });
                retString += sbretval.ToString();
            }

            return (retString, retNodes, retProject);
        }
        public (string retString, Dictionary<string, Node> retNodes, Ev3Project retProject) TraverseZip3(string zipName, OptionsClass options)
        {
            using (var fs = File.Open(zipName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                return TraverseZip3(fs, zipName, options);
            }
        }

        /// <summary>
        /// Traverse one zip file via file stream
        /// </summary>
        /// <param name="zipFileStream"></param>
        /// <param name="programName"></param>
        /// <param name="outputMode"></param>
        /// <param name="listOnly"></param>
        /// <param name="isTreeForGit"></param>
        /// <returns></returns>
        public (string retString, Dictionary<string, LogBlock> retBlocks, Ev3Project retProject) TraverseZip2(Stream zipFileStream, string zipName, OptionsClass options)
        {
            TraverseStart(zipFileStream, zipName, ref options);

            try
            {
                return TraverseZip_Internal();
            }
            finally
            {
                // close file
                TraverseEnd();
            }
        }

        /// <summary>
        /// implementation of zip traversal
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private (string retString, Dictionary<string, LogBlock> retBlocks, Ev3Project retProject) TraverseZip_Internal()
        {
            // update mode
            if (Options.UpdateProjectSortABC) UpdateProjectABC_();

            // if listing only, skip myblock reading
            if (!Options.ListOnly) m_ev3project = ExtractEV3Project();

            // if listing only, skip myblock reading
            if (Options.ListOnly) ConsoleExt.WriteLine("List of programs:", ConsoleColor.Gray);

            // handle zip nodes
            Dictionary<string, LogBlock> retdict = new Dictionary<string, LogBlock>();
            if (!Options.ProjectOnly)
            {
                // iterate through ev3p programs that match the name criteria if any
                List<ZipEntry> zipEntriesAll = m_zipFile.Cast<ZipEntry>()
                    .Where(ze => ze.Name.EndsWith(".ev3p"))
                    .OrderBy(ze => ze.Name)
                    .ToList();
                List<ZipEntry> zipEntries = zipEntriesAll
                    .Where(ze => (Options.SelectedProgram == null || ze.Name.ToLower().StartsWith(Options.SelectedProgram.ToLower())))
                    .ToList();
                foreach (ZipEntry zipEntry in zipEntries)
                {
                    if (Options.ListOnly)
                    {
                        ConsoleExt.WriteLine(string.Format("  {0}", zipEntry.Name.Replace(@".ev3p", string.Empty)), ConsoleColor.Gray);
                    }
                    else
                    {
                        if (zipEntries.Count > 1 || Options.SelectedProgram == null) Output.Common.Impl.AddSeparator();

                        // traverse a single program/file and output the node
                        LogBlock lbRoot = ProcessSingleZipEntry(retdict, zipEntry);
                    }
                }

                //if no programs found -> display error message
                if (zipEntries.Count == 0)
                {
                    ConsoleExt.WriteLine("No programs with such name were found.\n\nAvailable programs:\n" +
                        string.Join("\n", zipEntriesAll.Select(ze => ze.Name).ToArray()), ConsoleColor.Gray);
                }
            }

            // generate output (JSON, HTML) file
            var retstring = Output.Common.Impl.Flush(Options.JSONIndentedFormat);
            return (retstring, retdict, m_ev3project);
        }

        private LogBlock ProcessSingleZipEntry(Dictionary<string, LogBlock> retdict, ZipEntry zipEntry)
        {
            LogBlock lbRoot = TraverseOneZipEntry(zipEntry.Name);
            if (lbRoot == null) return null;

            retdict[zipEntry.Name] = lbRoot;
            Output.Common.Impl.Print(lbRoot, Options.IsTreeForGit);

            // execute metrics calculation 
            if (Options.DisplayMetrics) DisplayMetrics(zipEntry, lbRoot);
            return lbRoot;
        }

        private static void DisplayMetrics(ZipEntry zipEntry, LogBlock lbRoot)
        {
            Metrics metrics = Metrics.GenerateMetrics(lbRoot);
            ConsoleExt.Write($"Code metrics: {zipEntry.Name.Replace(".ev3p", "")} ", ConsoleColor.White);
            ConsoleExt.WriteLine(string.Join(", ",
                metrics.ToList()
                .OrderBy(kvp => kvp.Key).ToList()
                .Select(kvp => string.Format($"{kvp.Key}={kvp.Value}"))
                .ToArray()), ConsoleColor.Gray);
        }

        /// <summary>
        /// Sort open project alphabetically
        /// </summary>
        private void UpdateProjectABC_()
        {
            var ev3project = new Ev3Project();
            Tuple<XDocument, ZipEntry> result = ev3project.Update_SortEntriesAlphabetically(m_zipFile);
            Update_ZipEntryWithXDoc(m_zipFile, result.Item2, result.Item1);
            ConsoleExt.WriteLine("Updated EV3Project sorting programs, myblocks, media files and variables alphabetically.\n", ConsoleColor.White);
        }

        /// <summary>
        /// Traverse one zip file via stream and return tree (lib usage)
        /// </summary>
        /// <param name="zipFileStream"></param>
        /// <param name="zipName"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public string TraverseZip(Stream zipFileStream, string zipName, OptionsClass options)
        {
            (string retval, _, _) = TraverseZip2(zipFileStream, zipName, options);
            return retval;
        }
        public string TraverseZip(string zipName, OptionsClass options)
        {
            (string retval, _, _) = TraverseZip2(zipName, options);
            return retval;
        }

        /// <summary>
        /// Start the traversing, init all elements
        /// </summary>
        /// <param name="zipFileStream"></param>
        /// <param name="zipName_in"></param>
        /// <param name="options"></param>
        internal void TraverseStart(Stream zipFileStream, string zipName_in, ref OptionsClass options)
        {
            if (options == null) options = new OptionsClass();
            Options = options;
            Output.Common.DebugFlag = options.DebugFlag;
            Output.Common.SetInterface(options.OutputMode);
            if (options.OutputMode == OutputMode.Console) Printer.UseColors = !options.IsTreeForGit;

            // for GraphViz dot output use project tree only
            if (options.OutputMode == OutputMode.DOT)
            {
                options.ProjectOnly = true;
                options.SelectedProgram = null;
            }

            m_zipName = zipName_in;
            options.IsEV3Desktop = m_zipName.EndsWith("ev3"); //.ev3m is Ipad;
            Ev3FileTraverser = new Ev3FileTraverser();

            // open the zip file and process;
            m_zipFile = new ZipFile(zipFileStream);
        }

        /// <summary>
        /// End traversal, close all resources
        /// </summary>
        internal void TraverseEnd()
        {
            if (m_zipFile != null)
            {
                m_zipFile.Close();
                m_zipFile = null;
            }
        }

        /// <summary>
        /// Extract the EV3 Project from the generic project file
        /// </summary>
        /// <returns></returns>
        internal Ev3Project ExtractEV3Project()
        {
            Ev3Project ev3project = null;

            // iterate through MyBlock definitions
            foreach (ZipEntry zipEntry in m_zipFile.Cast<ZipEntry>().Where(ze => ze.Name.EndsWith(".ev3p.mbxml")))
            {
                Stream zipElementStream = m_zipFile.GetInputStream(zipEntry);
                Ev3FileTraverser.TraverseAndAddMyBlockDefinition(zipElementStream, zipEntry.Name.Replace(".mbxml", string.Empty));
            }

            // PROJECT: collect project statistics
            if (Options.SelectedProgram == null)
            {
                try
                {
                    ev3project = new Ev3Project();
                    LogBlock lgProjectRoot = ev3project.ProcessProject(m_zipFile, Path.GetFileName(m_zipName));
                    if (lgProjectRoot != null)
                    {
                        Output.Common.Impl.Print(lgProjectRoot, Options.IsTreeForGit);
                    }
                }
                catch (Exception e)
                {
                    // if there is an exception - only fail the current file
                    ConsoleExt.WriteLine($"Error processing project", ConsoleColor.Gray);
                    ConsoleExt.WriteLine(e.ToString(), ConsoleColor.Gray);

                    if (!Options.HandleAndReportErrors) throw; // re-throw is error handling is disabled
                    else ev3project = null;
                }
            }

            return ev3project;
        }

        /// <summary>
        /// Traverse One zip entry by name
        /// </summary>
        /// <param name="entryName"></param>
        /// <returns></returns>
        internal LogBlock TraverseOneZipEntry(string entryName)
        {
            ZipEntry zipEntry = m_zipFile.GetEntry(entryName);
            Stream zipElementStream = m_zipFile.GetInputStream(zipEntry);

            // process the program file
            List<LogBlock> lbLogBlocks = Ev3FileTraverser.TraverseFile(zipElementStream, entryName,
                new Ev3FileTraverser.OptionsClass()
                {
                    HandleAndReportErrors = Options.HandleAndReportErrors,
                    IsEV3Desktop = Options.IsEV3Desktop
                });
            if (lbLogBlocks == null) return null;
            LogBlock lbRoot = lbLogBlocks[0];

            // check project usage - if project was analyzed
            if (Options.GenerateExtraAttributes &&
                m_ev3project != null && m_ev3project.Dependencies_UsedBy.ContainsKey(entryName))
            {
                var usedBy = m_ev3project.Dependencies_UsedBy[entryName].Select(elem => Path.GetFileNameWithoutExtension(elem)).ToArray();
                lbRoot.Params.Add(LogBlock.CONST_ATTR_USEDBY,
                    new NodeParam(string.Join("|", usedBy.ToArray()))
                    {
                        ValueDetails = new { EV3Ps = usedBy.ToArray() },
                        ValueType = NodeParam.ParamValueTypes.EV3Ps
                    }
                    );
            }

            return lbRoot;
        }

        /// <summary>
        /// Project update one zip entry contents with xdoc
        /// </summary>
        /// <param name="zipFile"></param>
        /// <param name="ze"></param>
        /// <param name="xdoc"></param>
        private void Update_ZipEntryWithXDoc(ZipFile zipFile, ZipEntry ze, XDocument xdoc)
        {
            zipFile.BeginUpdate();
            zipFile.Delete(ze.Name);

            using (MemoryStream msEntry = new MemoryStream())
            {
                xdoc.Save(msEntry);

                CustomStaticDataSource sds = new CustomStaticDataSource();
                sds.SetStream(msEntry);
                zipFile.Add(sds, ze.Name);

                zipFile.CommitUpdate();
                //zipFile.IsStreamOwner = false;
                //zipFile.Close();
            }
        }

    }
}
