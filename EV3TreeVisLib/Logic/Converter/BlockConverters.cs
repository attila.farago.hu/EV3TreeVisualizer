﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace EV3TreeVisLib.Logic.Converter
{
    /// <summary>
    /// Block attribute display conversion for most common attribute types
    /// </summary>
    static class BlockConverter
    {
        /// <summary>
        /// Convert value based on block and param names
        /// </summary>
        /// <param name="nodetarget"></param>
        /// <param name="paramname"></param>
        /// <param name="value"></param>
        /// <param name="isArray"></param>
        /// <returns></returns>
        internal static string Convert(string nodetarget, string paramname, NodeParam valuep, bool isArray)
        {
            switch (nodetarget)
            {
                case LedColorBlockConverter.BLOCK_LedOnBlock when (paramname == LedColorBlockConverter.PARAM_Color):
                    return LedColorBlockConverter.Convert(paramname, valuep);

                case ColorBlockConverter.BLOCK_CompareBlock when (paramname == null || paramname == ColorBlockConverter.PARAM_Setofcolors):
                case ColorBlockConverter.BLOCK_ValueBlock when (paramname == null || paramname == ColorBlockConverter.PARAM_Setofcolors || paramname == ColorBlockConverter.PARAM_Case_Color):
                    return ColorBlockConverter.Convert(paramname, valuep, isArray);

                case BrickButtonBlockConverter.BLOCK_CompareBlock when (paramname == null || paramname == BrickButtonBlockConverter.PARAM_Buttons):
                case BrickButtonBlockConverter.BLOCK_ValueBlock when (paramname == null || paramname == BrickButtonBlockConverter.PARAM_Buttons || paramname == BrickButtonBlockConverter.PARAM_Case_Button):
                    return BrickButtonBlockConverter.Convert(paramname, valuep, isArray);

                case TouchBlockConverter.BLOCK_CompareBlock when paramname == TouchBlockConverter.PARAM_Compare:
                case TouchBlockConverter.BLOCK_ValueBlock when paramname == TouchBlockConverter.PARAM_Value:
                    return TouchBlockConverter.Convert(paramname, valuep, false);

                case string val when SoundBlockConverter.MatchBlock(nodetarget):
                    return SoundBlockConverter.Convert(paramname, valuep, isArray);

                default:
                    return valuep.Value = AttributeConverter.Convert(nodetarget, paramname, valuep.Value, valuep);
            }
        }
    }

    /// <summary>
    /// Convert Color blocks - Identification_Color.xml
    /// </summary>
    static class ColorBlockConverter
    {
        readonly static string[] NAMES = { "NoColor", "Black", "Blue", "Green", "Yellow", "Red", "White", "Brown" };
        public const string BLOCK_CompareBlock = "ColorCompare";
        public const string BLOCK_ValueBlock = "ColorValue";
        public const string PARAM_Setofcolors = "Setofcolors";
        public const string PARAM_Case_Color = "Color";

        internal static string ConvertSingle(string value)
        {
            if (Int32.TryParse(value, out int valueIdx))
                return value = NAMES[valueIdx];
            else
                return value;
        }

        internal static string Convert(string paramname, NodeParam valuep, bool isArray)
        {
            if (!isArray)
            {
                valuep.Value = ConvertSingle(valuep.Value);
                valuep.ValueType = NodeParam.ParamValueTypes.Color;
                valuep.ValueDetails = new { Color = valuep.Value };
                return valuep.Value;
            }
            else
            {
                var valuearray = valuep.Value.Trim('[', ']').Split(',').Select(s => ConvertSingle(s)).ToArray();
                valuep.Value = "[" +
                    string.Join(",", valuearray) +
                    "]";
                valuep.ValueDetails = new { Colors = valuearray };
                valuep.ValueType = NodeParam.ParamValueTypes.Colors;
                return valuep.Value;
            }
        }
    }

    /// <summary>
    /// Convert LedColor blocks - Identification_LEDColor.xml
    /// </summary>
    static class LedColorBlockConverter
    {
        readonly static string[] NAMES = { "Green", "Orange", "Red" };
        public const string BLOCK_LedOnBlock = "LedOn";
        public const string PARAM_Color = "Color";

        internal static string Convert(string paramname, NodeParam valuep)
        {
            valuep.ValueType = NodeParam.ParamValueTypes.LEDColor;
            if (Int32.TryParse(valuep.Value, out int valueIdx))
            {

                valuep.Value = NAMES[valueIdx];
                valuep.ValueDetails = new { Color = valuep.Value };
                return valuep.Value;
            }
            else
                return valuep.Value;
        }
    }

    /// <summary>
    /// Convert BrickButtons blocks - Identification_OutputBrickButtonID.xml
    /// </summary>
    static class BrickButtonBlockConverter
    {
        readonly static string[] NAMES = { "Nothing", "Left", "Center", "Right", "Top", "Bottom" };
        public const string BLOCK_CompareBlock = "ButtonCompare";
        public const string BLOCK_ValueBlock = "ButtonValue";
        public const string PARAM_Buttons = "Buttons";
        public const string PARAM_Case_Button = "Value";

        internal static string ConvertSingle(string value)
        {
            if (Int32.TryParse(value, out int valueIdx))
                return value = NAMES[valueIdx];
            else
                return value;
        }

        internal static string Convert(string paramname, NodeParam valuep, bool isArray)
        {
            if (!isArray)
            {
                valuep.ValueType = NodeParam.ParamValueTypes.OutputBrickButtonID;
                return valuep.Value = ConvertSingle(valuep.Value);
            }
            else
            {
                valuep.ValueType = NodeParam.ParamValueTypes.OutputBrickButtons;
                var valuearray = valuep.Value.Trim('[', ']').Split(',').Select(s => ConvertSingle(s)).ToArray();
                valuep.Value = "[" +
                    string.Join(",", valuearray) +
                    "]";
                valuep.ValueDetails = new { Buttons = valuearray };
                return valuep.Value;
            }
        }
    }

    /// <summary>
    /// Convert Touch block - Identification_TouchState.xml
    /// </summary>
    static class TouchBlockConverter
    {
        readonly static string[] NAMES = { "Released", "Pressed", "Bumped" };
        public const string BLOCK_CompareBlock = "TouchCompare";
        public const string BLOCK_ValueBlock = "TouchValue";
        public const string PARAM_Compare = "Pressed,ReleasedorBumped";
        public const string PARAM_Value = "State";

        private static string ConvertInt(string value)
        {
            if (Int32.TryParse(value, out int valueIdx))
                return NAMES[valueIdx];
            else
                return value;
        }
        internal static string Convert(string paramname, NodeParam valuep, bool isArray)
        {
            if (!isArray)
            {
                valuep.ValueType = NodeParam.ParamValueTypes.TouchState;
                if (int.TryParse(valuep.Value, out int valueIdx))
                    return valuep.Value = NAMES[valueIdx];
                else
                    return valuep.Value;
            }
            else
            {
                valuep.ValueType = NodeParam.ParamValueTypes.TouchStates;
                valuep.Value = "[" +
                    string.Join(",", valuep.Value.Trim('[', ']').Split(',').Select(s => ConvertInt(s)).ToArray()) +
                    "]";
                return valuep.Value;
            }
        }
    }

    /// <summary>
    /// Convert Touch block - Identification_TouchState.xml
    /// </summary>
    static class SoundBlockConverter
    {
        readonly static string[] PLAYTYPE_NAMES = { "WaitForCompletion", "PlayOnce", "Repeat" };
        readonly static string[] BLOCKS = { "PlayTone", "PlayNote", "PlaySoundFile" };
        public const string PLAYTYPE_PARAM = "PlayType";

        internal static bool MatchBlock(string block)
        {
            return BLOCKS.Contains(block);
        }

        internal static string Convert(string paramname, NodeParam valuep, bool isArray = false)
        {
            if (paramname == PLAYTYPE_PARAM)
            {
                valuep.ValueType = NodeParam.ParamValueTypes.PlayType;
                if (int.TryParse(valuep.Value, out int valueIdx))
                    return valuep.Value = PLAYTYPE_NAMES[valueIdx];
            }
            return valuep.Value;
        }
    }


    #region attribute converters
    /// <summary>
    /// Generic attribute converter
    /// </summary>
    static class AttributeConverter
    {
        public const string PARAM_Port_Input = "Port";
        public const string PARAM_MotorPort_Input = "MotorPort";
        public const string PARAM_MotorPorts_Input = "Ports";
        public const string PARAM_Direction_Input = "Direction";
        readonly static string[] DIRECTION_NAMES = { "Increase", "Decrease", "Any" };
        internal const string PARAM_Comparison_Input = "Comparison";
        readonly static string[] COMPARISON_NAMES = { "equals", "!=", ">", ">=", "<", "<=" };
        internal const string PARAM_Comparison2_Input = "Comparison2";
        readonly static string[] COMPARISON2_NAMES = { "equals", "!=" };

        public static string Convert(string block, string paramname, string value, NodeParam valuep)
        {
            switch (paramname)
            {
                case PARAM_Port_Input:
                    {
                        //1.1=>1  1.2=>2
                        var re = new Regex(@"^1\.(?=[1-4])");
                        value = re.Replace(value, "");
                    }
                    break;
                case PARAM_MotorPort_Input:
                case PARAM_MotorPorts_Input:
                    {//1.A=>A   1.D=>D   1.B+C=>B+C
                        var re = new Regex(@"^1\.(?=[A,B,C,D]\+?[A,B,C,D]?)");
                        value = re.Replace(value, "");
                    }
                    break;
                case PARAM_Direction_Input:
                    {
                        if (int.TryParse(value, out int idx))
                            value = DIRECTION_NAMES[idx];
                    }
                    break;
                case PARAM_Comparison_Input:
                    {
                        if (int.TryParse(value, out int idx))
                            value = COMPARISON_NAMES[idx];
                    }
                    break;
                case PARAM_Comparison2_Input:
                    {
                        if (int.TryParse(value, out int idx))
                            value = COMPARISON2_NAMES[idx];
                    }
                    break;
            }
            return value;
        }
    }
    #endregion
}
