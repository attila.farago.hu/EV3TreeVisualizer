﻿using EV3ModelLib;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace EV3TreeVisLib.Logic.Converter
{
    /// <summary>
    /// Block name converter and shortener
    /// </summary>
    public static partial class BlockNameConverter
    {
        /// <summary>
        /// Block name unescaping
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static string UnescapeBlockName(string blockName)
        {
            return new Regex(@"\\(.)").Replace(blockName, "$1");
        }

        internal const string CONST_Display_BlockFamily = "BlockFamily";
        internal const string CONST_Display_IconName = "IconName";

        internal const string CONST_BlockFamily_FORK = Node.BLOCK_Fork;
        internal const string CONST_BlockFamily_CONNECTOR = LogBlock.ExtraBlocks.CONNECTOR;
        internal const string CONST_BlockFamily_EMPTY = Node.BLOCK_Empty;
        internal const string CONST_BlockFamily_FlowControl = "FlowControl";
        internal const string CONST_BlockFamily_Action = "Action";
        internal const string CONST_BlockFamily_Sensor = "Sensor";
        internal const string CONST_BlockFamily_DataOperations = "DataOperations";
        internal const string CONST_BlockFamily_Advanced = "Advanced";
        internal const string CONST_BlockFamily_MyBlocks = "MyBlocks";

        internal const string CONST_BlockTarget_CommentBlock = @"CommentBlock\.vix";
        internal const string CONST_BlockTarget_DataLogMaster = @"X3Placeholder_2A058539\-ED76\-4476\-93FE\-CCE8AA559C5A_DataLogMasterSingle\.vix";
        internal const string CONST_BlockTarget_ConfigurableWhileLoop = @"ConfigurableWhileLoop";
        internal const string CONST_BlockName_Switch_PairedConfigurableMethodCall = @"PairedConfigurableMethodCall";
        internal const string CONST_BlockName_ConfigurableWaitFor = @"ConfigurableWaitFor";

        /// <summary>
        /// Return the display details for a block
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static LegoBlockRecord GetBlockDisplayDetails(string blockName)
        {
            blockName = UnescapeAndTruncVIXExtension(blockName);
            if (BlockTranslateMap.ContainsKey(blockName)) return BlockTranslateMap[blockName];

            var retval = new BlockNameConverter.LegoBlockRecord();
            switch (blockName)
            {
                case string val when blockName.Contains(".ev3p"):
                    // myblock colors
                    string icon = DictMyBlockIcons.ContainsKey(blockName) ? DictMyBlockIcons[blockName] : null;
                    if (icon != null)
                    {
                        retval.BlockFamily = CONST_BlockFamily_MyBlocks; // MYBLOCKs have icon
                        retval.IconName = icon;
                    }
                    else
                    {
                        retval.IconName = "icon_extra_program.png";
                    }
                    break;
                case LogBlock.ExtraBlocks.FORK:
                case LogBlock.ExtraBlocks.FORKITEM:
                    {
                        retval.BlockFamily = CONST_BlockFamily_FORK;
                        retval.IconName = "icon_extra_fork.png";
                    }
                    break;
                case LogBlock.ExtraBlocks.CONNECTOR:
                    {
                        retval.BlockFamily = CONST_BlockFamily_CONNECTOR;
                        retval.IconName = "icon_extra_connector.png";
                    }
                    break;
                case LogBlock.ExtraBlocks.EMPTY:
                    {
                        retval.BlockFamily = CONST_BlockFamily_EMPTY;
                        retval.IconName = "icon_extra_empty.png";
                    }
                    break;
                case string val when blockName.EndsWith(".ev3"):
                    {
                        retval.IconName = "icon_extra_project.png";
                    }
                    break;
                case LogBlock.ExtraBlocks.PROJECT_SOUNDS:
                    {
                        retval.IconName = "icon_extra_projectsounds.png";
                    }
                    break;
                case LogBlock.ExtraBlocks.PROJECT_IMAGES:
                    {
                        retval.IconName = "icon_extra_projectimages.png";
                    }
                    break;
                case LogBlock.ExtraBlocks.PROJECT_VARS:
                    {
                        retval.IconName = "icon_extra_projectvariables.png";
                    }
                    break;
                case LogBlock.ExtraBlocks.PROJECT_WARNINGS:
                    {
                        retval.IconName = "icon_extra_projectwarnings.png";
                    }
                    break;
            }
            return retval;
        }

        /// <summary>
        /// truncate name to a friendly name
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        internal static string UnescapeAndTruncVIXExtension(string blockName)
        {
            return UnescapeBlockName(blockName)
                .Replace(@".vix", string.Empty)
                .Replace(@".gvi", string.Empty);
        }

        /// <summary>
        /// truncate name to a friendly name
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static string TruncName(string blockName)
        {
            return UnescapeBlockName(blockName)
                .Replace(@".vix", string.Empty)
                .Replace(@".gvi", string.Empty)
                .Replace(@".ev3p", string.Empty)
                .Replace(@"X3.Lib:", string.Empty)
                .Replace(@"X3Placeholder_2A058539-ED76-4476-93FE-CCE8AA559C5A_", string.Empty) //MathEquation
                ;
        }

        /// <summary>
        /// translate blockname to friendly name
        /// </summary>
        /// <param name="blockName"></param>
        /// <returns></returns>
        public static string Translate(string blockName)
        {
            BlockNameConverter.LegoBlockRecord blockDisplayDetails = GetBlockDisplayDetails(blockName);
            if (blockDisplayDetails != null && !string.IsNullOrEmpty(blockDisplayDetails.ShortName)) return blockDisplayDetails.ShortName;
            return TruncName(blockName);
        }

        /// <summary>
        /// return color for a block (based on family)
        /// </summary>
        /// <param name="sBlockFamily"></param>
        /// <returns></returns>
        public static string GetColorForFamily(string blockFamily)
        {
            return blockFamily != null && BlockFamilyColors.ContainsKey(blockFamily) ? BlockFamilyColors[blockFamily] : null;
        }

        /// <summary>
        /// Add custome my block icons for display
        /// </summary>
        public static void AddDictMyBlockIcons(string filename, string iconfile)
        {
            DictMyBlockIcons[filename] = iconfile;
        }
        private static Dictionary<string, string> DictMyBlockIcons = new Dictionary<string, string>();

        /// <summary>
        /// Block family colors
        /// </summary>
        internal static Dictionary<string, string> BlockFamilyColors = new Dictionary<string, string>
        {
            [CONST_BlockFamily_Action] = "#73B939",
            [CONST_BlockFamily_FlowControl] = "#FEAB26",
            [CONST_BlockFamily_Sensor] = "#ECD407",
            [CONST_BlockFamily_DataOperations] = "#E32F00",
            [CONST_BlockFamily_Advanced] = "#0022B2",
            [CONST_BlockFamily_MyBlocks] = "#009A9A",

            [CONST_BlockFamily_FORK] = "#777777",
            [CONST_BlockFamily_CONNECTOR] = "#9400D3",
            [CONST_BlockFamily_EMPTY] = "#dddddd",
        };
    }
}
