using EV3ModelLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EV3TreeVisLib.Logic.Converter
{
    public static partial class BlockNameConverter
    {
        public class LegoBlockRecord
        {
            public string ShortName { get; set; }
            public string Reference { get; set; }
            public string IconName { get; set; }
            public string BlockFamily { get; set; }
        }

        static BlockNameConverter()
        {
            BlockTranslateMap = BlockInfo.BlockMapByRef.ToDictionary(bi => bi.Key, bi => new LegoBlockRecord()
            {
                ShortName = bi.Value.ShortName,
                Reference = bi.Value.Reference,
                BlockFamily = bi.Value.BlockFamily,
                IconName = bi.Value.IconName
            });
        }

        public static readonly Dictionary<string, LegoBlockRecord> BlockTranslateMap;
    }
}
