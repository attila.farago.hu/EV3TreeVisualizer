﻿using EV3TreeVisLib.Logic.Converter;
using EV3TreeVisLib.Printing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Wmhelp.XPath2;

namespace EV3TreeVisLib.Logic
{
    /// <summary>
    /// Data Wire registry to keep track of data wires name and scope
    ///     and handle situations level-only endpint Is sonnected to an output tunnel directly.
    ///     
    /// </summary>
    internal class DataWireRegistry
    {
        internal Dictionary<string, List<Tuple<LogBlock, bool?>>> translationTableScope2Blocks;
        internal Dictionary<string, string> translationTableDw2Scope;

        /// <summary>
        /// ctor
        /// </summary>
        internal DataWireRegistry()
        {
            translationTableDw2Scope = new Dictionary<string, string>();
            translationTableScope2Blocks = new Dictionary<string, List<Tuple<LogBlock, bool?>>>();
        }

        /// <summary>
        /// Check if Wire already has global translation in dw (except for root output) 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private bool HasWireInScope(string key)
        {
            if (translationTableScope2Blocks.ContainsKey(key))
            {
                var elem = translationTableScope2Blocks[key];
                return elem != null &&
                    !ignoredScopeWires.Contains(key);
            }
            return false;
        }

        /// <summary>
        /// Public facade of wire translation routine
        /// top.w3 -> dw3
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="wire"></param>
        /// <param name="lb"></param>
        /// <param name="canCreate"></param>
        /// <returns></returns>
        internal string TranslateWireInScope(string scope, string wire, LogBlock lb, string sDataType, bool? bIncoming,
            bool canCreate = true, string sPostfix = null)
        {
            return TranslateWireInScopeImpl(scope, wire, lb, sDataType, bIncoming, canCreate, null, sPostfix);
        }

        /// <summary>
        /// Translate Data Wire Type
        /// </summary>
        /// <param name="sDataType"></param>
        /// <returns></returns>
        public static string TranslateWireDataTypeToDisplay(string sDataType)
        {
            if (TranslateWireDataTypeInt.ContainsKey(sDataType ?? string.Empty))
                return TranslateWireDataTypeInt[sDataType];
            return string.Empty;
        }
        private static readonly Dictionary<string, string> TranslateWireDataTypeInt = new Dictionary<string, string>
        {
            ["String"] = "S",
            ["Single"] = "N",
            ["Single[]"] = "NA",
            ["Boolean"] = "B",
            ["Boolean[]"] = "BA",
        };

        /// <summary>
        /// Implementation for wire translation routine
        /// top.w3 -> dw3
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="wire"></param>
        /// <param name="lb"></param>
        /// <param name="canCreate"></param>
        /// <param name="translatedWireInput"></param>
        /// <returns></returns>
        private string TranslateWireInScopeImpl(string scope, string wire, LogBlock lb, string sDataType, bool? bIncoming,
            bool canCreate, string translatedWireInput = null, string sPostfix = null)
        {
            string translatedWire;
            string fullname = GetFullName(scope, wire);

            // generate or get translatedwire id
            if (!translationTableDw2Scope.TryGetValue(fullname, out translatedWire))
            {
                if (!canCreate) return null;
                translationTableScope2Blocks[fullname] = new List<Tuple<LogBlock, bool?>>();

                if (translatedWireInput == null)
                {
                    int count = 0;
                    const string POSTFIX_ALPHABET = "abcdefghjklmnpqrstuvwxyz";
                    while (true)
                    {
                        translatedWire = "d" + TranslateWireDataTypeToDisplay(sDataType) + wire +
                            (count++ > 0 ? POSTFIX_ALPHABET[(count - 2) % POSTFIX_ALPHABET.Length].ToString() : string.Empty)
                            + sPostfix;
                        // avoid duplication - this global wire id should not exist yet
                        if (!translationTableDw2Scope.ContainsValue(translatedWire)) break;
                    }
                }
                else
                {
                    translatedWire = translatedWireInput;
                }
                translationTableDw2Scope.Add(fullname, translatedWire);
            }
            translationTableScope2Blocks[fullname].Add(Tuple.Create<LogBlock, bool?>(lb, bIncoming));

            return translatedWire;
        }

        /// <summary>
        /// fullly specified name of a scope and wire
        /// </summary>
        /// <param name="scope"></param>
        /// <param name="wire"></param>
        /// <returns></returns>
        internal static string GetFullName(string scope, string wire)
        {
            return scope + "." + wire;
        }

        #region Tunnel Translation
        Dictionary<bool, Dictionary<string, string>> tunnelTranslationScope2Table = new Dictionary<bool, Dictionary<string, string>>()
            {{true,new Dictionary<string, string>() }, {false,new Dictionary<string, string>() } };
        Dictionary<bool, Dictionary<string, string>> tunnelTranslationScope1Table = new Dictionary<bool, Dictionary<string, string>>()
            {{true,new Dictionary<string, string>() }, {false,new Dictionary<string, string>() } };
        Dictionary<string, List<string[]>> directShortcutNodes = new Dictionary<string, List<string[]>>();
        internal class WireTunnelConnection
        {
            internal bool DirectionIn { get; set; }
            internal string DataWire { get; set; }
        }

        /// <summary>
        /// Add wire translation for wire Tunnels 
        /// </summary>
        /// <param name="lb1"></param>
        /// <param name="scope1"></param>
        /// <param name="wire1"></param>
        /// <param name="lb2"></param>
        /// <param name="scope2"></param>
        /// <param name="wire2"></param>
        /// <returns></returns>
        internal string AddWireTunnelTranslation(LogBlock lb1, string scope1, string wire1, LogBlock lb2, string scope2, string wire2, bool? bDirection, string sDataType)
        {
            // assume scope1 is already processed scope1 => scope2 
            // TODO: check if assumption is OK
            // add same dw to scope2.wire2
            //what if dwid2 exists already and not dwid1

            string dwid = null;
            string key1 = GetFullName(scope1, wire1);
            string key2 = GetFullName(scope2, wire2);
            bool direction = WireTunnelTranslationIsIncoming(scope1, wire1, scope2, wire2);
            dwid = TranslateWireInScopeImpl(scope1, wire1, lb1, sDataType, null, true); //TODO: add datatyle
            TranslateWireInScopeImpl(scope2, wire2, lb1, sDataType, null, true, dwid, null); //!!lb2 //TODO: add datatyle
            //if (direction)
            //{
            //// incoming tunnel
            //dwid = TranslateWireInScopeImpl(scope1, wire1, null, false);
            //TranslateWireInScopeImpl(scope2, wire2, null, true, dwid);
            // not all cases handle the same wiring - can be wrong for switches, could be good for loops
            //}
            //else
            //{
            //    // outgoing tunnel
            //    dwid = TranslateWireInScopeImpl(scope2, wire2, null, false);
            //    TranslateWireInScopeImpl(scope1, wire1, null, true, dwid);
            //}
            // incoming or outgoing applies only for a certain scope/tunnel batch - needs explicit clearing
            tunnelTranslationScope2Table[direction][key2] = dwid;
            tunnelTranslationScope1Table[direction][key1] = dwid;

            //!! tunnel name must contain case ID as well top.n17.D15.w7 not only top.n17.w11 -- cases can have the same wire ids! :(
            if (Output.Common.DebugFlag)
            {
                Console.WriteLine($"tunnel: {scope1}.{wire1} -> {scope2}.{wire2}  ==  {dwid}");
            }

            return dwid;
        }

        /// <summary>
        /// Check if current scope is incoming
        /// </summary>
        /// <param name="scope1"></param>
        /// <param name="wire1"></param>
        /// <param name="scope2"></param>
        /// <param name="wire2"></param>
        /// <returns></returns>
        internal bool WireTunnelTranslationIsIncoming(string scope1, string wire1, string scope2, string wire2)
        {
            // wire is incoming global datawire is not registered yet (except for myblock root outgoing node)
            string key1 = GetFullName(scope1, wire1);
            string key2 = GetFullName(scope2, wire2);
            if (!HasWireInScope(key1) || tunnelTranslationScope1Table[false].ContainsKey(key1))
            {
                // outoing from current scope to inside
                return false;
            }
            else
            {
                // incoming to current scope from outside
                return true;
            }
        }

        /// <summary>
        /// Add to ignored scope wires (megaaccessor output - not known from sequence yet)
        /// </summary>
        internal void AddIgnoredScopeWire(string scopewire)
        {
            ignoredScopeWires.Add(scopewire);
        }
        List<string> ignoredScopeWires = new List<string>();

        /// <summary>
        /// Collect and Add tunnel shortcut nodes
        /// </summary>
        /// <param name="thisnode"></param>
        /// <param name="lbCurrent"></param>
        /// <param name="aTunnel2Datawire"></param>
        /// <param name="sb2"></param>
        internal void CollectAndAddTunnelShortcutNodes(XElement thisnode, LogBlock lbCurrent, Dictionary<string, Tuple<string, string>> aTunnel2Datawire, List<LogBlock> sb2)
        {
            // collect tunnel shortcut nodes
            var shortcutNodeList = GetTunnelShortcutNodes(aTunnel2Datawire, thisnode, lbCurrent);

            // add shortcut / direct wires - nodes
            //foreach (var shortcutNode in shortcutNodes)
            //{
            foreach (var elem in shortcutNodeList)
            {
                string dwid1 = elem.Item1;
                string dwid2 = elem.Item2;
                LogBlock lbTunnelShortcut = new LogBlock(null)
                {
                    Block = LogBlock.ExtraBlocks.CONNECTOR,
                    BlockType = LogBlock.BlockTypes.Connector,
                    BlockDisplayDetails = BlockNameConverter.GetBlockDisplayDetails(LogBlock.ExtraBlocks.CONNECTOR),
                    Parent = lbCurrent
                };
                lbTunnelShortcut.Params.Add("from", new NodeParam(dwid1, dwid1));
                lbTunnelShortcut.Params.Add("to", new NodeParam(dwid2, dwid2));
                sb2.Add(lbTunnelShortcut);
            }
        }

        /// <summary>
        /// Process and collect Tunnel datawire data
        /// </summary>
        /// <param name="lbCurrent"></param>
        /// <param name="pairedNode"></param>
        /// <param name="bExtendScope"></param>
        /// <param name="sTunnelSelector"></param>
        /// <returns></returns>
        internal Dictionary<string, Tuple<string, string>> ProcessTunnels(LogBlock lbCurrent, XElement pairedNode, bool bExtendScope, string sTunnelSelector)
        {
            // Handle wire conversions in during CASE Tunnels for translation
            //<ConfigurableCaseStructure.ConfigurableCaseTunnel Id="b40" Terminals="n37=w28, D38=w49, D39=w15" Bounds="144 1166 30 63" />
            // incoming w28 ==> D38.Case Id=D38 -> caseNode 
            // can be any combination if wire is not used Terminals="n37=w28, D38, D39=" or Terminals="n37=w28, D38, D39=w15"

            var aTunnels = pairedNode.XPath2SelectElements(sTunnelSelector);
            var aTunnel2Datawire = new Dictionary<string, Tuple<string, string>>();
            foreach (XElement aTunnelNode in aTunnels)
            {
                string sTerminal = aTunnelNode.XPath2SelectOne("string(@Terminals)").ToString();
                string sTunnelId = aTunnelNode.XPath2SelectOne("string(@Id)").ToString();
                MatchCollection matches = new Regex(@"(?:(?<node>\D\d+)=(?<value>w\d+))(?:, ){0,1}").Matches(sTerminal);
                string sSourceNodeId = matches[0].Groups[1].ToString();
                string sSourceWireId = matches[0].Groups[2].ToString();
                string fullid1 = lbCurrent.Scope + "." + sSourceWireId;
                aTunnel2Datawire[sTunnelId] = Tuple.Create<string, string>(lbCurrent.Scope, sSourceWireId);
                // find wire id at source node
                //!!
                //var sJointEnding = $"N({sTunnelId}:{pairedId})"; //N(b38:n39)"  
                //var wire = rootNode.XPath2SelectElement(@"*:Wire[contains(@Joints,'" + sJointEnding + "')]");
                //string sJoints = wire.Attribute("Joints").Value;
                //string[] resp = Regex.Split(sJoints, @"^N\((\w\d+):.*N\("+sTunnelId+ @":.+\)|^N\("+sTunnelId+ @":.*N\((\w\d+):.+\)");
                //if (resp.Count() < 3) { 
                //    int ax = 1;
                //}
                //var sOriginNodeId = resp[1];
                //var xOriginNode = rootNode.XPath2SelectElement(@"*[@Id='" + sOriginNodeId + "']");
                //var xTerminalNode = xOriginNode.XPath2SelectElement(@"*:ConfigurableMethodTerminal/*:Terminal[@Wire='" + sSourceWireId + "']");
                //var bDirection = xTerminalNode.Attribute("Direction").Value == "Input";
                //var sDataType = xTerminalNode.Attribute("DataType").Value;

                foreach (Match match in matches.OfType<Match>().Skip(1))
                {
                    string sTargetNodeId = match.Groups[1].ToString();
                    string sTargetWireId = match.Groups[2].ToString();
                    // extend scope with D# switch_fork nodes for switch - loops do not need this
                    string scope2 = lbCurrent.FullId + (bExtendScope ? "." + sTargetNodeId : string.Empty);
                    string fullid2 = scope2 + "." + sTargetWireId;

                    // check if data wire tunnel is a shortcut / direct connection
                    //TODO: check and add second node
                    AddWireTunnelTranslation(lbCurrent, lbCurrent.Scope, sSourceWireId,
                        null,
                        //sTargetNodeId, 
                        scope2, sTargetWireId, null, null); //!! !bDirection, sDataType);
                                                            //!! no need to check tunnel here, will do below at case

                    //!! can check for input type and direction
                    //Console.WriteLine($"{sTunnelId}: {fullid1} --> {fullid2}");
                }
            }
            return aTunnel2Datawire;
        }

        /// <summary>
        /// Collect Tunnel Shortcut nodes for Loops and Switches
        /// </summary>
        /// <param name="aTunnel2Datawire"></param>
        /// <param name="xmlParentXmlNode"></param>
        /// <param name="lbParentNode"></param>
        /// <returns></returns>
        private List<Tuple<string, string>> GetTunnelShortcutNodes(Dictionary<string, Tuple<string, string>> aTunnel2Datawire, XElement xmlParentXmlNode, LogBlock lbParentNode)
        {
            // check tunnel direct shortcuts
            var shortcutNodeList = new List<Tuple<string, string>>();
            {
                //<Wire Id="w5" Joints="N(b38:D41) v(-4) h(80) N(b40:D41)" />
                foreach (var wireNode in xmlParentXmlNode.XPath2SelectElements(@"*:Wire"))
                {
                    string sJoints = wireNode.Attribute("Joints").Value;
                    // check if there is a wire that "shortcuts" - connecting two tunnels of the same pseudonode
                    //      remark: wire can be connected to more nodes as well
                    //      <Wire Id="w10" Joints="N(b18:D16) v(-85) h(31) N(b17:D16)" />
                    //      <Wire Id="w8" Joints="N(b29:D29) v(-64) h(31) N(b26:D29) B(0) v(-42) h(-100) N(n32:valueIn)" />
                    //      <Wire Id="w6" Joints="N(b28:D30) N(n35:valueIn) B(0) v(-16) h(62) N(b26:D30)" />
                    //      <Wire Id="w11" Joints="N(b14:D15) v(-36) h(62) N(b17:D15)" />
                    //      <Wire Id="w5" Joints="N(b23:D22) v(-4) h(44) N(b24:D22)" />
                    // pseudonode can be a 
                    //      case node e.g. "D21" - ConfigurableCaseStructure.Case Id="D21"
                    //      pseudo loop node "D22", "d0" - DiagramId="d0", DiagramId="D22"
                    Regex re = new Regex(@"^N\((?<tunnel1>b\d+):(?<pseudonode>[Dd]\d+)\).*N\((?<tunnel2>b\d+):\k'pseudonode'\)");
                    Match m = re.Match(sJoints);
                    if (m != null && m.Length > 0)
                    {
                        //shortcut from tunnel1 -> tunnel2
                        var a = m.Groups[0];
                        var tunnel1Id = m.Groups[1].Value;
                        //var tunnelCaseId = m.Groups[2].Value; // must be same as caseId
                        var tunnel2Id = m.Groups[3].Value;

                        var scopewire1 = aTunnel2Datawire[tunnel1Id];
                        var scopewire2 = aTunnel2Datawire[tunnel2Id];

                        var dwire1 = TranslateWireInScope(scopewire1.Item1, scopewire1.Item2, lbParentNode, null, null, true);
                        var dwire2 = TranslateWireInScope(scopewire2.Item1, scopewire2.Item2, lbParentNode, null, null, true);

                        shortcutNodeList.Add(Tuple.Create<string, string>(dwire1, dwire2));
                    }
                }
            }

            return shortcutNodeList;
        }

        #endregion // Tunnel Translation

        #region Debugging functions
        /// <summary>
        /// Print all data wires for Debug purposes
        /// </summary>
        internal void Debug_PrintContentsToConsole()
        {
            if (translationTableDw2Scope.Count > 0)
            {
                ConsoleExt.WriteLine("DataWireRegistry:", ConsoleColor.White);
                foreach (KeyValuePair<string, string> translationkvp in translationTableDw2Scope)
                {
                    ConsoleExt.Write($"  {translationkvp.Key.Replace("root.", "")} = {translationkvp.Value}  ", ConsoleColor.Gray);
                    ConsoleExt.WriteLine(
                        string.Join(", ",
                        translationTableDw2Scope
                        .Where(elem => elem.Value == translationkvp.Value && elem.Key != translationkvp.Key)
                        .Select(elem => elem.Key.Replace("root.", ""))), ConsoleColor.DarkGray);
                }
            }
        }
        #endregion // Debugging functions
    }
}
