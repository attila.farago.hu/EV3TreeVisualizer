﻿using EV3ModelLib;
using EV3TreeVisLib.Logic.Converter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3TreeVisLib.Logic
{
    internal class LogBlock2ModelConverter : INodeConverter
    {
        static LogBlock2ModelConverter()
        {
            RegisterConverter();
        }

        public static void RegisterConverter()
        {
            NodeConversion.RegisterConverter(typeof(LogBlock2ModelConverter));
        }

        public Node Convert(object logblock)
        {
            if (!(logblock is LogBlock)) return null;

            return ConvertBlock(logblock as LogBlock, null);
        }

        private Parameter ConvertParam(Node node, string name, NodeParam param, bool bCallDirectionInput, 
            bool bForceMyBlockWire = false)
        {
            var retval = new Parameter(name, param?.RawValue, node) { IsInput = bCallDirectionInput };

            if (param?.WireId != null)
                retval.Variable = param?.WireId;
            else if (bForceMyBlockWire) // force variables in case of myBlocks
                retval.Variable = retval.Name;

            return retval;
        }

        private Node ConvertBlock(LogBlock lblock1, Node parent)
        {
            var refname = lblock1.BlockDisplayDetails?.Reference;

            Node node1 = new Node()
            {
                Name = lblock1.Block, //!!
                Parent = parent,
                SourceObj = lblock1,
                RefName = refname
            };


            switch (lblock1.BlockType)
            {
                case LogBlock.BlockTypes.NativeSwitch:
                    node1.HasSwitch = true; node1.Name = node1.Name.Replace(Node.BLOCK_SwitchPrefix, null);
                    break;
                case LogBlock.BlockTypes.NativeWait:
                    node1.HasWait = true; node1.Name = node1.Name.Replace(Node.BLOCK_WaitForPrefix, null);
                    break;
                case LogBlock.BlockTypes.NativeEV3P:
                    node1.NodeType = Node.NodeTypeEnum.MyBlock;
                    break;
                case LogBlock.BlockTypes.ForkParent:
                    node1.NodeType = Node.NodeTypeEnum.ForkParent;
                    break;
                case LogBlock.BlockTypes.ForkItem:
                    node1.NodeType = Node.NodeTypeEnum.ForkItem;
                    break;
                case LogBlock.BlockTypes.CaseItem:
                    node1.NodeType = Node.NodeTypeEnum.CaseItem;
                    break;
                case LogBlock.BlockTypes.Connector:
                    node1.NodeType = Node.NodeTypeEnum.Connector;
                    break;
                case LogBlock.BlockTypes.Empty:
                    node1.NodeType = Node.NodeTypeEnum.Empty;
                    break;

                //case LogBlock.BlockTypes.Top:
                case LogBlock.BlockTypes.Root:
                    if (lblock1.IsMyBlock)
                    {
                        node1.NodeType = Node.NodeTypeEnum.MyBlock;
                        //TODO: save iconname somehow
                    }
                    break;

                case LogBlock.BlockTypes.ProjectRoot:
                case LogBlock.BlockTypes.ProjectExtraImages:
                case LogBlock.BlockTypes.ProjectExtraSounds:
                case LogBlock.BlockTypes.ProjectExtraVariables:
                case LogBlock.BlockTypes.ProjectExtraWarnings:
                    //NOOP
                    break;

                case LogBlock.BlockTypes.Native:
                default:
                    if (lblock1.Block == BlockNameConverter.CONST_BlockTarget_ConfigurableWhileLoop && lblock1.Params.ContainsKey(Node.PARAM_LOOP_StopCondition))
                    {
                        node1.NodeType = Node.NodeTypeEnum.Loop;
                        string sStopCondition = lblock1.Params[Node.PARAM_LOOP_StopCondition].Value;
                        lblock1.Params.Remove(Node.PARAM_LOOP_StopCondition);
                        node1.Name = sStopCondition;
                        //remove stop attribute
                    }
                    break;
            };

            //-- convert parameters
            var paramsTemp = new List<KeyValuePair<int, Parameter>>();
            if (refname != null)
            {
                BlockInfo.BlockMapByRef.TryGetValue(refname, out var refblockDescriptor);

                var paramDefList = refblockDescriptor?.Params?.OrderBy(pa => !pa.Value.CallDirectionInput).ThenBy(pa => pa.Value.CallIndex).ToList();
                foreach (var paramdef1 in paramDefList)
                {
                    //-- no output parameters here
                    if ((node1.HasSwitch || node1.NodeType.In(Node.NodeTypeEnum.CaseItem, Node.NodeTypeEnum.Loop)) && !paramdef1.Value.CallDirectionInput) continue;
                    if (node1.HasWait && !paramdef1.Value.CallDirectionInput && paramdef1.Value.IsResult) continue;

                    lblock1.Params.TryGetValue(paramdef1.Key, out NodeParam value1);
                    Parameter param2 = ConvertParam(node1, paramdef1.Key, value1, paramdef1.Value.CallDirectionInput);

                    //int.TryParse(paramdef1.Value.CallIndex, out var callindex);
                    node1.Parameters.Add(paramdef1.Key, param2);
                    //!! apply to pattern, read callindex
                }
            }

            //-- add any left over params (for non official blocks)
            foreach (var param1 in lblock1.Params.Where(elem => !node1.Parameters.ContainsKey(elem.Key)))
            {
                var bInput = param1.Value.IsInput;
                Parameter param2 = ConvertParam(node1, param1.Key, param1.Value, bInput, lblock1.IsMyBlock);
                node1.Parameters[param1.Key] = param2;
            }

            //-- reorder params
            //paramsTemp
            //        .OrderBy(pt => pt.Key)
            //                    .ToList()
            //                    .ForEach(elem => node1.Parameters.Add(elem.Value.Name, elem.Value));


            //-- add children
            if (lblock1.Children?.Count > 0)
            {
                lblock1.Children.ForEach(lbsub =>
                {
                    var nodesub = ConvertBlock(lbsub, node1);
                    //if (block1.HasSwitch) nodesub.NodeType = Node.NodeTypeEnum.CaseItem;
                    node1.Children.Add(nodesub);
                });
            }

            return node1;
        }
    }
}
