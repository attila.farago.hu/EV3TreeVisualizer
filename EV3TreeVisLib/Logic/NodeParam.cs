﻿using EV3ModelLib;
using Newtonsoft.Json;

namespace EV3TreeVisLib.Logic
{
    /// <summary>
    /// Node parameter value including data wire id - if any
    /// </summary>
    public class NodeParam
    {
        public NodeParam(string value, string wireId = null)
        {
            this.Value = this.RawValue = UnescapeParamValue(value);
            this.WireId = wireId;
        }

        /// <summary>
        /// Value of the parameter
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Original raw value
        /// </summary>
        public string RawValue { get; set; }

        /// <summary>
        /// Data wire id - if any
        /// </summary>
        public string WireId { get; set; }

        /// <summary>
        /// Details - if any
        /// </summary>
        public dynamic ValueDetails { get; set; }

        /// <summary>
        /// Parameter Value type
        /// </summary>
        public ParamValueTypes? ValueType { get; set; } = null;
        public enum ParamValueTypes
        {
            Color = 0, Colors = 1, TouchState = 2, TouchStates = 3, LEDColor = 4, PlayType = 5, OutputBrickButtonID = 6, OutputBrickButtons = 7, EV3Ps = 8,
            MotorPort = 9, MotorPorts = 10, SensorPort = 11, WaitForChange = 12, Comparison = 13, Comparison2 = 14,
            IROutputButtonID = 15,
            RGFbase64 = 255
        }

        /// <summary>
        /// Is input
        /// </summary>
        public bool IsInput { get; internal set; } = true;

        /// <summary>
        /// Unescape the parameter value
        /// </summary>
        /// <param name="argvalue"></param>
        /// <returns></returns>
        private string UnescapeParamValue(string argvalue)
        {
            argvalue = argvalue.Replace(@"\ ", string.Empty); // multi word params
                                                              // InterruptsToListenFor_, i, InterruptValue, LoopIterationCount --> Int32 dataypes with "0" value to skip
            if (argvalue.EndsWith(Node.PARAM_WiredSpecial_Src)) argvalue = Node.PARAM_WiredSpecial; // Wired port - need simplification only if not connected with data wire already - should not happen
            return argvalue;
        }
    }
}
