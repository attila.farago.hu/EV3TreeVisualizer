﻿using EV3ModelLib;
using System.Collections.Generic;

namespace EV3TreeVisLib.Logic
{
    /// <summary>
    /// Collection of parameters for a node
    /// </summary>
    public class NodeParams : Dictionary<string, NodeParam>
    {
        /// <summary>
        /// Constructor with parent node
        /// </summary>
        /// <param name="parent"></param>
        public NodeParams(LogBlock parent)
        {
            this.Parent = parent;
        }

        /// <summary>
        /// Parent Node
        /// </summary>
        LogBlock Parent { get; }

        /// <summary>
        /// Add a parameter with value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Add(string key, NodeParam value)
        {
            key = UnescapeAndShortenParamName(key);
            base.Add(key, value);
        }

        /// <summary>
        /// Add a parameter from string value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddString(string key, string value)
        {
            this.Add(key, new NodeParam(value, null));
        }

        public static string UnescapeAndShortenParamName(string argvalue) => Utils.UnescapeAndShortenParamName(argvalue);
    }
}
