﻿using EV3TreeVisLib.Logic;
using Medallion.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EV3TreeVisLib
{
    /// <summary>
    /// Code metrics
    /// </summary>
    public class Metrics : Dictionary<string, int>
    {
        public static Metrics GenerateMetrics(LogBlock lbRoot)
        {
            // Code complexity metrics
            //
            // max depth
            // max number of cases
            // # data wires
            // # nodes
            // variability # different blocks / # blocks
            Metrics metrics = new Metrics();
            int maxdepth = 0;
            int maxbranches = 0;
            int maxforks = 0;
            int numnodes = 0;
            int nodetypes = 0;
            HashSet<string> metrics_blocktype_hash = new HashSet<string>();
            var tree = Traverse.DepthFirst(Tuple.Create<LogBlock, int>(lbRoot, 0),
                tup_lb_depth =>
                {
                    LogBlock lb = tup_lb_depth.Item1;
                    int depth = tup_lb_depth.Item2;
                    maxdepth = Math.Max(maxdepth, tup_lb_depth.Item2);

                    switch (lb.BlockType)
                    {
                        case LogBlock.BlockTypes.NativeSwitch:
                            maxbranches = Math.Max(maxbranches, lb.Children?.Count ?? 0);
                            break;
                        case LogBlock.BlockTypes.ForkParent:
                            maxforks = Math.Max(maxforks, lb.Children?.Count ?? 0);
                            break;
                    }
                    if (lb.BlockType <= LogBlock.BlockTypes.Native) metrics_blocktype_hash.Add(lb.Block);

                    if (lb.Children == null) return new List<Tuple<LogBlock, int>>();

                    int newdepth = lb.IsBlockTypeNative ? depth + 1 : depth;
                    return tup_lb_depth.Item1.Children.Select(lb1 => Tuple.Create<LogBlock, int>(lb1, newdepth)).ToList();
                }).ToList();
            //var maxdept = tree.Max(tup_lb_depth => tup_lb_depth.Item2);
            numnodes = tree.Where(tup => tup.Item1.IsBlockTypeNative).Count();
            nodetypes = metrics_blocktype_hash.Count;

            metrics["MaxDepth"] = maxdepth;
            metrics["MaxBranches"] = maxbranches;
            metrics["MaxForks"] = maxforks;
            metrics["Nodes"] = numnodes;
            metrics["NodeTypes"] = nodetypes;
            metrics["NodeTypeVariability"] = numnodes > 0 ? (int)((double)nodetypes * 100 / numnodes) : 0;

            return metrics;
        }
    }
}
