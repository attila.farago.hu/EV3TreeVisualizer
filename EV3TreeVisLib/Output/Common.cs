﻿using EV3TreeVisLib.Logic;
using EV3TreeVisLib.Printing;

namespace EV3TreeVisLib.Output
{
    public static class Common
    {
        /// <summary>
        /// Output additional debug information
        /// </summary>
        internal static IOutputInterface Impl { get; private set; }
        internal static bool DebugFlag { get; set; }
        internal static void SetInterface(OutputMode mode)
        {
            Printer.SetMode(mode);

            switch (mode)
            {
                case OutputMode.Console:
                case OutputMode.HTML:
                case OutputMode.Text:
                    Impl = new Output.Console();
                    break;
                case OutputMode.JSON:
                    Impl = new Output.JSON();
                    break;
                case OutputMode.DOT:
                    Impl = new Output.DotGraphViz();
                    break;
                default:
                case OutputMode.None:
                    Impl = new Output.None();
                    break;
            }
        }
    }

    /// <summary>
    /// interface for Output handling
    /// </summary>
    public interface IOutputInterface
    {
        void Print(LogBlock logBlock, bool isTreeForGit);
        string Flush(bool useJSONIndentedFormat);
        void AddSeparator();
    }
}
