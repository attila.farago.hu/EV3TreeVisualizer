﻿using EV3TreeVisLib.Logic;
using EV3TreeVisLib.Logic.Converter;
using EV3TreeVisLib.Printing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace EV3TreeVisLib.Output
{
    internal class Console : IOutputInterface
    {
        /// <summary>
        /// Print LogBlock hierarchy to Console
        /// </summary>
        /// <param name="logBlocks"></param>
        public void Print(LogBlock logBlock, bool isTreeForGit)
        {
            // add anchor
            Printer.AddAnchor(logBlock.Block);

            PrintLogBlockToConsole(logBlock, new bool[] { }, isTreeForGit);
        }

        /// <summary>
        /// Print LogBlock hierarchy to Console
        /// </summary>
        /// <param name="logBlocks"></param>
        /// <param name="isParentLastChildArr"></param>
        /// <param name="isTreeForGit"></param>
        private void PrintLogBlocksToConsole(List<LogBlock> logBlocks, bool[] isParentLastChildArr,
           bool isTreeForGit)
        {
            // format while printing
            foreach (LogBlock lbIter in logBlocks)
            {
                bool isThisLastKid = logBlocks.Last() == lbIter;
                PrintLogBlockToConsole(lbIter, isParentLastChildArr, isTreeForGit, isThisLastKid);
            }
        }

        /// <summary>
        /// Print LogBlock hierarchy to Console
        /// </summary>
        /// <param name="lbIter"></param>
        /// <param name="isParentLastChildArr"></param>
        /// <param name="isTreeForGit"></param>
        /// <param name="isThisLastKid"></param>
        private void PrintLogBlockToConsole(LogBlock lbIter, bool[] isParentLastChildArr,
           bool isTreeForGit,
           bool isThisLastKid = true)
        {
            const KnownColor defaultColor = KnownColor.DarkGray;
            bool[] isParentLastChildArrNew = isParentLastChildArr;

            // check if there is only one main sequence line (all others have only 1 elements)
            bool showParallelSingleBlockIndent = false;
            bool skipThisSingleBlock = false;
            const bool IsOptimizeParallelSimpleBlocks = true;
            const bool IsOptimizeParallelLastBlock = true;
            if (IsOptimizeParallelSimpleBlocks)
            {
                if (lbIter.BlockType == LogBlock.BlockTypes.ForkParent)
                {
                    // always skip the fork parent node
                    //bool hasSingleFork = lbIter.Children.Count(elem => elem.Children.Count > 1) <= 1;
                    //if (hasSingleFork)
                    skipThisSingleBlock = true;
                }
                else if (lbIter.BlockType == LogBlock.BlockTypes.ForkItem)
                {
                    // skip the fork item node if parent has only one fork branch with more than one chidren 
                    // (only single parallel branches exist)
                    // fork branch should be collapsed/simplified if it has sub-chidren (e.g. loop or fork nodes)
                    bool hasSingleFork =
                        lbIter.Parent.Children.Count(elem => elem.Children?.Count > 1 || (elem.Children?.First()?.Children?.Count > 1))
                        <= 1;
                    if (hasSingleFork)
                        if (lbIter.Children.Any() && (!lbIter.Children?.First()?.Children?.Any() ?? true))
                            skipThisSingleBlock = true;
                }
                else if (lbIter.Parent?.BlockType == LogBlock.BlockTypes.ForkItem)
                {
                    // collape the node if its parent is a fork item node with parent having only one fork branch 
                    // with more than one chidren (only single parallel branches exist)
                    // fork branch should be collapsed/simplified if it has sub-chidren (e.g. loop or fork nodes)
                    bool hasSingleFork = lbIter.Parent.Parent.Children.Count(elem => elem.Children.Count > 1) <= 1;
                    if (hasSingleFork)
                    {
                        bool aLevelIsSingleParallelForkItemButNotLast = lbIter.Parent.Children.Count == 1;
                        if (aLevelIsSingleParallelForkItemButNotLast)
                            if (lbIter.Parent != lbIter.Parent.Parent.Children.Last() &&
                                (!lbIter.Children?.Any() ?? true))
                                showParallelSingleBlockIndent = true;
                    }
                }
            }

            if (IsOptimizeParallelLastBlock)
            {
                if (lbIter.BlockType == LogBlock.BlockTypes.ForkParent) skipThisSingleBlock = true;
                if (lbIter.BlockType == LogBlock.BlockTypes.ForkItem && isThisLastKid) skipThisSingleBlock = true;
            }

            // display the single block with indent
            if (!skipThisSingleBlock)
            {
                // format while printing
                if (Common.DebugFlag)
                {
                    Printer.Write(string.Format("{0,-4} ", lbIter.Id), defaultColor);
                }

                // print prefix for tree generation
                isParentLastChildArrNew = isParentLastChildArr.Concat(new bool[1] { isThisLastKid }).ToArray();
                StringBuilder sb = new StringBuilder();
                // skip level 0 as it contains only main program block
                for (int i = 1; i < isParentLastChildArrNew.Length; i++)
                {
                    bool aLastKidOnLevel = isParentLastChildArrNew[i];
                    bool isThisLastLevel = i == isParentLastChildArr.Length;
                    sb.Append(
                            (aLastKidOnLevel ?
                                    (isThisLastLevel ?
                                        (!isTreeForGit ?
                                            (!showParallelSingleBlockIndent ? " └" : " ├") :
                                            " │") :
                                        "  ")
                                    : (isThisLastLevel ? " ├" : " │")));

                    if (Common.DebugFlag)
                    {
                        if (isThisLastLevel)
                            sb.Append(lbIter.WireIn != null ? lbIter.WireIn.PadRight(4, '_') : "____");
                    }

                    sb.Append(
                        isThisLastLevel ?
                            (showParallelSingleBlockIndent ? "F+" : "─ ") : " "
                        );
                }
                if (sb.Length > 0) Printer.Write(sb.ToString(), KnownColor.Gray);

                // print block name with appropriate family color
                string color = lbIter.GetColor();

                dynamic extra = null;
                if (lbIter.BlockType == LogBlock.BlockTypes.NativeEV3P)
                {
                    var blockname = lbIter.Block;
                    extra = new { before = $"<a href='#{blockname}'>", after = "</a>" };
                }
                Printer.Write(lbIter.Block, color ?? "gray", extra);
                Printer.Write(" ");
                if (lbIter.Params != null)
                {
                    foreach (var kvp in lbIter.Params)
                    {
                        string[] colorListAttribute = null;
                        string colorAttribute = null;

                        //if (kvp.Value.ValueDetails!=null !="Color")
                        switch (kvp.Value.ValueType)
                        {
                            case NodeParam.ParamValueTypes.Color:
                            case NodeParam.ParamValueTypes.Colors:
                            case NodeParam.ParamValueTypes.LEDColor:
                                {
                                    Printer.Write(kvp.Key + "=", defaultColor);
                                    try { colorListAttribute = kvp.Value.ValueDetails.Colors; } catch { }
                                    try { colorAttribute = kvp.Value.ValueDetails.Color; } catch { }
                                    if (colorListAttribute != null)
                                    {
                                        var last = ((IEnumerable<string>)colorListAttribute).Last();
                                        Printer.Write("[", defaultColor);
                                        foreach (var colorvalue in colorListAttribute)
                                        {
                                            //skip black for console
                                            Printer.Write(colorvalue,
                                                !Printer.ColorToAvoid.Equals(colorvalue, StringComparison.OrdinalIgnoreCase) ? colorvalue : "darkgray");
                                            if (last != colorvalue) Printer.Write(",", defaultColor);
                                        }
                                        Printer.Write("]", defaultColor);
                                    }
                                    else if (colorAttribute != null)
                                    {
                                        Printer.Write(kvp.Value.Value,
                                            !Printer.ColorToAvoid.Equals(colorAttribute, StringComparison.OrdinalIgnoreCase) ? colorAttribute : "darkgray");
                                    }
                                }
                                break;

                            case NodeParam.ParamValueTypes.EV3Ps:
                                Printer.Write(kvp.Key + "=", defaultColor);
                                string[] ev3ps = null;
                                try { ev3ps = kvp.Value.ValueDetails.EV3Ps; } catch { }
                                if (ev3ps != null)
                                {
                                    foreach (var ev3p in ev3ps)
                                    {
                                        string blockname = ev3p;
                                        Printer.Write(ev3p, defaultColor, new { before = $"<a href='#{blockname}'>", after = "</a>" });
                                        if (ev3ps.Last() != ev3p) Printer.Write(" | ", defaultColor);
                                    }
                                }
                                break;

                            case NodeParam.ParamValueTypes.RGFbase64:
                                {
                                    Printer.Write(kvp.Key, defaultColor, new
                                    {
                                        after = $" <a id='img_{lbIter.Block}'><img src='{kvp.Value.Value}'></a>"
                                    });
                                }
                                break;

                            default:
                                // consider color for ipad command boxes
                                if (kvp.Key == "Comment" && lbIter.Block == LogBlock.ExtraBlocks.COMMENTBOX && lbIter.Params.ContainsKey("Color"))
                                {
                                    Printer.Write(kvp.Key + "=", defaultColor);
                                    string color1 = lbIter.Params["Color"].ValueDetails.Color;
                                    Printer.Write(kvp.Value.Value, color1);
                                }
                                else if (lbIter.Block == "Display.File" && kvp.Key == "Filename")
                                {
                                    Printer.Write(kvp.Key + "=", defaultColor);
                                    Printer.Write(kvp.Value.Value, defaultColor,
                                        new { before = $"<a href='#img_{kvp.Value.Value}' class='displayfile'>", after = "</a>" }
                                        );
                                }
                                else
                                {
                                    Printer.Write(kvp.Key + "=" + kvp.Value.Value, defaultColor);
                                }
                                break;
                        }
                        if (lbIter.Params.Last().Key != kvp.Key) Printer.Write(" | ", defaultColor);
                    }
                    //Printer.Write(string.Join(" | ", from kvp in lbIter.Params select kvp.Key + "=" + kvp.Value.Value), defaultColor);
                }
                Printer.WriteLine();
            }

            // iterate on children nodes
            if (lbIter.Children != null)
            {
                PrintLogBlocksToConsole(lbIter.Children, isParentLastChildArrNew, isTreeForGit);
            }
        }

        /// <summary>
        /// Print separator line
        /// </summary>
        public void AddSeparator()
        {
            Printer.WriteLine(new String('=', 80), KnownColor.DarkCyan);
        }

        /// <summary>
        /// Flush output
        /// </summary>
        /// <param name="useJSONIndentedFormat"></param>
        /// <returns></returns>
        public string Flush(bool useJSONIndentedFormat)
        {
            return Printer.PopBuffer();
        }

    }
}
