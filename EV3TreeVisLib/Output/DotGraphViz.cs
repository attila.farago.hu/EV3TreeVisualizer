﻿using EV3TreeVisLib.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3TreeVisLib.Output
{
    internal class DotGraphViz : IOutputInterface
    {
        /// <summary>
        /// log blocks collection for final print
        /// </summary>
        private List<LogBlock> CollectedLogBlocks { get; } = new List<LogBlock>();

        public void AddSeparator()
        {
            // NOOP
        }

        public string Flush(bool useJSONIndentedFormat)
        {
            string sResult = PrintLogBlockTreeToDot(CollectedLogBlocks.FirstOrDefault());

            return sResult;
        }

        /// <summary>
        /// Collect the blocks
        /// </summary>
        /// <param name="logBlock"></param>
        /// <param name="isTreeForGit"></param>
        public void Print(LogBlock logBlock, bool isTreeForGit)
        {
            CollectedLogBlocks.Add(logBlock);
        }

        /// <summary>
        /// Generate GraphViz DOT diagram
        /// </summary>
        /// <param name="lbNode"></param>
        /// <returns></returns>
        private string PrintLogBlockTreeToDot(LogBlock lbNode)
        {
            //graphviz model
            List<string> sb = new List<string>();
            sb.Add(@"digraph ev3graph { rankdir=LR; size=12.5; node[shape=box]; ranksep=1;");
            sb.Add("graph [fontname = \"helvetica\"]; node[fontname = \"helvetica\"]; edge[fontname = \"helvetica\"];");
            void traverseNode(LogBlock lb)
            {
                if (lb.Children == null) return;

                // ipad ev3m has only one node, no subprograms
                if (!lb.Children.Any() && lb == lbNode) sb.Add($"\"{lb.Block}\"");

                foreach (var lb2 in lb.Children)
                {
                    // skip any "project extra" blocks added
                    if (lb2.BlockType >= LogBlock.BlockTypes.ProjectExtraImages) continue;

                    // dependency line
                    var line = $"\"{lb.Block}\" -> \"{lb2.Block}\"";

                    // avoid duplicate dependencies especially for hierarchical myblocks
                    if (!sb.Contains(line)) sb.Add(line);

                    // traverse all children
                    traverseNode(lb2);
                }
            }
            if (lbNode != null)
                traverseNode(lbNode);
            sb.Add("}");
            return string.Join(Environment.NewLine, sb.ToArray());
        }
    }
}
