﻿using EV3TreeVisLib.Logic;
using EV3TreeVisLib.Logic.Converter;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace EV3TreeVisLib.Output
{
    internal class JSON : IOutputInterface
    {
        /// <summary>
        /// log blocks collection for final print
        /// </summary>
        private List<LogBlock> CollectedLogBlocks { get; } = new List<LogBlock>();

        /// <summary>
        /// Print LogBlock hierarchy
        /// JSON will return all programs at once - this implies collection of the elements for the later Flush
        /// </summary>
        /// <param name="logBlocks"></param>
        /// <returns></returns>
        public void Print(LogBlock lbRoot, bool isTreeForGit)
        {
            CollectedLogBlocks.Add(lbRoot);
        }

        /// <summary>
        /// Print separator line
        /// </summary>
        public void AddSeparator()
        {
            // NOOP
        }

        /// <summary>
        /// Serialize LogBlocks hierarchy to JSON
        /// </summary>
        /// <param name="logBlocks"></param>
        /// <returns></returns>
        public string Flush(bool useJSONIndentedFormat)
        {
            LogBlockDecorateForJSON(CollectedLogBlocks);
            //var dummy = JArray.FromObject(logBlocks);
            string retval = JsonConvert.SerializeObject(CollectedLogBlocks, useJSONIndentedFormat ? Formatting.Indented : Formatting.None,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

            CollectedLogBlocks.Clear();
            return retval;
        }

        /// <summary>
        /// Decorate LogBlocks for Serialization to JSON
        /// </summary>
        /// <param name="logBlocks"></param>
        private void LogBlockDecorateForJSON(List<LogBlock> logBlocks)
        {
            foreach (var lb in logBlocks)
            {
                //Printer.WriteLine(lb);
                BlockNameConverter.LegoBlockRecord details = lb.BlockDisplayDetails;
                if (details != null)
                {
                    lb.Display = new Dictionary<string, string>();
                    string color = lb.GetColor();
                    if (color != null) lb.Display["Color"] = color;
                    if (!string.IsNullOrEmpty(lb.BlockDisplayDetails.IconName))
                        lb.Display["Icon"] = lb.BlockDisplayDetails.IconName;
                }
                if (lb.Children != null) LogBlockDecorateForJSON(lb.Children);
            }
        }
    }
}
