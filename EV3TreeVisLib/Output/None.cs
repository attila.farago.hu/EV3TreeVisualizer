﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EV3TreeVisLib.Logic;

namespace EV3TreeVisLib.Output
{
    public class None : IOutputInterface
    {
        public void AddSeparator()
        {
            // NOOP
        }

        public string Flush(bool useJSONIndentedFormat)
        {
            // NOOP
            return null;
        }

        public void Print(LogBlock logBlock, bool isTreeForGit)
        {
            // NOOP
        }
    }
}
