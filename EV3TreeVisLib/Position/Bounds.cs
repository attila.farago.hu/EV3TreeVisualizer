﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;
using Wmhelp.XPath2;

namespace EV3TreeVisLib.Position
{
    /// <summary>
    /// Bounds structure for Node Bounds
    /// </summary>
    [DebuggerDisplay("{Anchor.X} {Anchor.Y} {Width} {Height}")]
    internal class Bounds
    {
        internal Point Anchor { get; set; } = new Point(0, 0);
        internal double Width { get; set; }
        internal double Height { get; set; }
        public Bounds(Point anchor, double Width, double Height)
        {
            this.Anchor = anchor;
            this.Width = Width; this.Height = Height;
        }
        public Bounds(XElement xmlnode)
        {
            if (xmlnode == null) throw new ArgumentNullException();
            var bounds = xmlnode.Attribute("Bounds"); if (bounds == null) return;
            var arr = bounds.Value.Split(' ').Select(double.Parse).ToArray();
            this.Anchor = new Point(arr[0], arr[1]);
            this.Width = arr[2];
            this.Height = arr[3];
        }

        /// <summary>
        /// Create bounds for an XML element with absolute - shift by all ancestors
        /// </summary>
        /// <param name="xmlnode"></param>
        /// <returns></returns>
        internal static Bounds BoundsFromXElementWithShift(XElement xmlnode, PositionCache Cache = null)
        {
            if (xmlnode == null) return null;
            if (Cache != null && Cache.BoundsCache.ContainsKey(xmlnode)) return Cache.BoundsCache[xmlnode];
            try
            {
                var bnd = new Bounds(xmlnode);

                // iterate through parent
                XElement node = xmlnode;
                while (true)
                {
                    var parentWithBounds = node.XPath2SelectElement("..[@Bounds]"); //BlockDiagram has no Bounds
                    if (parentWithBounds == null) break;
                    var bounds2 = BoundsFromXElementWithShift(parentWithBounds);
                    if (bounds2 == null) break;
                    bnd.ShiftWith(bounds2);
                    node = parentWithBounds;
                }
                if (Cache != null) Cache.BoundsCache[xmlnode] = bnd;
                return bnd;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Create bounds for an XML element
        /// </summary>
        /// <param name="xmlnode"></param>
        /// <returns></returns>
        public static Bounds BoundsFromXElement(XElement xmlnode)
        {
            return xmlnode == null ? new Bounds(xmlnode) : null;
        }

        /// <summary>
        /// Shift with another bounds
        /// </summary>
        /// <param name="bounds2"></param>
        public void ShiftWith(Bounds bounds2)
        {
            Anchor.X += bounds2.Anchor.X;
            Anchor.Y += bounds2.Anchor.Y;
        }

        /// <summary>
        /// String converstion
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Anchor.X} {Anchor.Y} {Width} {Height}";
        }
    }
}
