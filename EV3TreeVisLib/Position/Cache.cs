﻿using EV3TreeVisLib.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Wmhelp.XPath2;

namespace EV3TreeVisLib.Position
{
    internal class PositionCache
    {
        /// <summary>
        /// Initialize the cache
        /// </summary>
        internal void Initialize()
        {
            BoundsCache = new Dictionary<XElement, Bounds>();
            BoundsLbCache = new Dictionary<LogBlock, Bounds>();
        }

        /// <summary>
        /// Create bounds for a LogBlock
        /// </summary>
        /// <param name="lb"></param>
        /// <returns></returns>
        internal Bounds BoundsLogBlock(LogBlock lb)
        {
            if (!BoundsLbCache.ContainsKey(lb)) BoundsLbCache.Add(lb, BoundsFromXElementWithShift(lb.Element));

            return BoundsLbCache[lb];
        }
        internal Dictionary<LogBlock, Bounds> BoundsLbCache = null;

        /// <summary>
        /// Create bounds for an XML element with absolute - shift by all ancestors
        /// </summary>
        /// <param name="xmlnode"></param>
        /// <returns></returns>
        internal Bounds BoundsFromXElementWithShift(XElement xmlnode)
        {
            return Bounds.BoundsFromXElementWithShift(xmlnode, this);
        }
        internal Dictionary<XElement, Bounds> BoundsCache = null;
    }
}
