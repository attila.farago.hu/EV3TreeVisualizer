﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3TreeVisLib.Printing
{
    internal static class ColorConverter
    {
        /// <summary>
        /// Converts the specified <see cref="Color"/> to it's nearest <see cref="Color"/> equivalent.
        /// </summary>
        /// <remarks>Original code taken from ColorExtensions package - added Hue bias</remarks>
        public static ConsoleColor ToNearestColor(this Color color)
        {
            ConsoleColor closestColor = 0;
            double delta = double.MaxValue;

            foreach (ConsoleColor consoleColor in Enum.GetValues(typeof(ConsoleColor)))
            {
                string consoleColorName = Enum.GetName(typeof(ConsoleColor), consoleColor);
                consoleColorName = string.Equals(consoleColorName, nameof(ConsoleColor.DarkYellow), StringComparison.Ordinal) ?
                    nameof(Color.Orange) : consoleColorName;
                Color rgbColor = Color.FromName(consoleColorName);
                double sum =
                    Math.Pow(rgbColor.GetHue() - color.GetHue(), 2.0) * 4 +
                    Math.Pow(rgbColor.R - color.R, 2.0) + Math.Pow(rgbColor.G - color.G, 2.0) + Math.Pow(rgbColor.B - color.B, 2.0);

                if (sum == 0.0)
                {
                    return consoleColor;
                }
                else if (sum < delta)
                {
                    delta = sum;
                    closestColor = consoleColor;
                }
            }

            return closestColor;
        }
    }
    }
