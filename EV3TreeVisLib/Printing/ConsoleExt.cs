﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV3TreeVisLib.Printing
{
    /// <summary>
    /// Wraps around the System.Console class, adding enhanced styling functionality.
    /// </summary>
    public static class ConsoleExt
    {
        public static void Write(string value, ConsoleColor? color = null)
        {
            WriteInColor(Console.Write, value, color);
        }
        public static void WriteLine(string value, ConsoleColor? color = null)
        {
            WriteInColor(Console.WriteLine, value, color);
        }
        public static void WriteLine()
        {
            Console.WriteLine();
        }
        public static void SetColor(ConsoleColor color)
        {
            System.Console.ForegroundColor = color;
        }

        private static void WriteInColor(Action<string> action, string value, ConsoleColor? color)
        {
            ConsoleColor oldSystemColor = System.Console.ForegroundColor;
            if (color.HasValue) System.Console.ForegroundColor = color.Value;
            action.Invoke(value);
            if (color.HasValue) System.Console.ForegroundColor = oldSystemColor;
        }
    }
}