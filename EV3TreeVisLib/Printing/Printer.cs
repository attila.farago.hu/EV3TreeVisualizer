﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;

namespace EV3TreeVisLib.Printing
{
    /// <summary>
    /// IPrinter interface for line printing output - console or HTML
    /// </summary>
    interface IPrinter
    {
        void Write(String value, Color? color = null, dynamic extra = null);
        void WriteLine(String value, Color? color = null);
        void WriteLine();
        string PopBuffer();
        void AddAnchor(string anchor_name);
    }

    /// <summary>
    /// Helper class to print to Console directly
    /// </summary>
    internal class ConsoleExtImpl : IPrinter
    {
        private int pagingcount = 0;
        private bool usepaging = true;

        public void AddAnchor(string anchor_name)
        {
            //NOOP
        }
        public string PopBuffer()
        {
            //NOOP
            return null;
        }
        public void Write(string value, Color? color = null, dynamic extra = null)
        {
            ConsoleColor? ccolor = null; if (color != null) ccolor = ColorConverter.ToNearestColor(color.Value);
            ConsoleExt.Write(value, ccolor);
            if (value != null) CheckPaging(value.Count(c => c == '\n'));
        }
        public void WriteLine(string value, Color? color = null)
        {
            ConsoleColor? ccolor = null; if (color != null) ccolor = ColorConverter.ToNearestColor(color.Value);
            ConsoleExt.WriteLine(value, ccolor);
            if (value != null) CheckPaging(value.Count(c => c == '\n') + 1);
        }
        public void WriteLine()
        {
            ConsoleExt.WriteLine();
            CheckPaging(1);
        }

        /// <summary>
        /// Check paging for lines advanced
        /// </summary>
        /// <param name="lines"></param>
        private void CheckPaging(int lines)
        {
            // paging
            if (usepaging)
            {
                if (Console.IsOutputRedirected) { usepaging = false; return; } //do not use paging if output is redirected

                pagingcount += lines;
                if (pagingcount >= Console.WindowHeight - 4)
                {
                    ConsoleExt.Write("-- More --", ConsoleColor.White);
                    ConsoleExt.Write(" (space = advance 1 page, enter = advance without paging, q = quit) ", ConsoleColor.DarkGray);

                    // wait for user input key press 
                    while (true)
                    {
                        var key = Console.ReadKey(true);

                        // space = advance 1 page, enter = advance without paging, q = quit 
                        if (key.KeyChar == 'q') Environment.Exit(0);
                        if (key.Key == ConsoleKey.Enter) { usepaging = false; break; }
                        if (key.KeyChar == ' ') break;
                    }
                    // clear the prompt
                    int currentLineCursor = Console.CursorTop;

                    // clearline - move to line start and clear it
                    Console.SetCursorPosition(0, currentLineCursor);
                    Console.WriteLine(new string(' ', Console.BufferWidth));
                    Console.SetCursorPosition(0, currentLineCursor);

                    pagingcount = 0;
                }
            }
        }
    }

    /// <summary>
    /// Helper class to print to String buffer
    /// </summary>
    internal class TextBufferImpl : IPrinter
    {
        private static StringBuilder sb;

        public TextBufferImpl()
        {
            sb = new StringBuilder();
        }

        public void AddAnchor(string anchor_name)
        {
            //NOOP
        }

        public string PopBuffer()
        {
            var retval = sb.ToString();
            sb.Clear();
            return retval;
        }

        public void Write(string value, Color? color = null, dynamic extra = null)
        {
            sb.Append(value);
        }

        public void WriteLine(string value, Color? color = null)
        {
            sb.AppendLine(value);
        }

        public void WriteLine()
        {
            sb.AppendLine();
        }
    }


    /// <summary>
    /// Helper class to print to HTML output
    /// </summary>
    internal class HTMLOutputPrintImpl : IPrinter
    {
        private static StringBuilder sb;
        public HTMLOutputPrintImpl()
        {
            string sResult = "<pre>";
            sb = new StringBuilder(sResult);
        }

        public void Write(string value, Color? color = null, dynamic extra = null)
        {
            if (extra != null) try { sb.Append(extra.before); } catch { }
            WriteInColor(s => sb.Append(s), value, color);
            if (extra != null) try { sb.Append(extra.after); } catch { }
        }

        public void WriteLine(string value, Color? color = null)
        {
            WriteInColor(s => sb.AppendLine(s), value, color);
        }

        public void WriteLine()
        {
            sb.AppendLine();
        }

        private static void WriteInColor(Action<string> action, string value, Color? color = null)
        {
            string ccolor = null; if (color != null) ccolor = System.Drawing.ColorTranslator.ToHtml(color.Value);
            action.Invoke(
                (ccolor != null ? "<span style='color:" + ccolor + "'>" : null) +
                value.ToString() +
                (ccolor != null ? "</span>" : null));
        }

        public string PopBuffer()
        {
            var retval = sb.ToString();
            sb.Clear();
            return retval;
        }
        public void AddAnchor(string anchor_name)
        {
            sb.Append($"<a id='{anchor_name}'></a>");
        }
    }

    /// <summary>
    /// Wraps around the System.Console class, adding enhanced styling functionality.
    /// </summary>
    public static class Printer
    {
        public static bool UseColors { get; set; } = true;

        public static string ColorToAvoid { get; set; } = "black";
        private static IPrinter printerImpl = null;

        /// <summary>
        /// Set printer mode based on output mode
        /// </summary>
        /// <param name="mode"></param>
        public static void SetMode(OutputMode mode)
        {
            switch (mode)
            {
                case OutputMode.Console:
                    printerImpl = new ConsoleExtImpl();
                    Printer.ColorToAvoid = "black";
                    break;
                case OutputMode.Text:
                    printerImpl = new TextBufferImpl();
                    Printer.ColorToAvoid = "black";
                    break;
                case OutputMode.HTML:
                    printerImpl = new HTMLOutputPrintImpl();
                    Printer.ColorToAvoid = "white";
                    break;
                default:
                    //NOOP
                    break;
            }
        }

        public static void Write(string value, KnownColor? color, dynamic extra = null)
        {
            Color? ccolor = null; if (UseColors && color != null) ccolor = Color.FromKnownColor(color.Value);
            Write(value, ccolor, extra);
        }
        public static void Write(string value, string color = null, dynamic extra = null)
        {
            Color? ccolor = null; if (UseColors && color != null) ccolor = ColorFromHtmlSafe(color, Color.DarkGray);
            printerImpl.Write(value, ccolor, extra);
        }
        public static void Write(string value, Color? color, dynamic extra = null)
        {
            printerImpl.Write(value, color, extra);
        }
        public static void WriteLine(string value, KnownColor? color = null)
        {
            Color? ccolor = null; if (UseColors && color != null) ccolor = Color.FromKnownColor(color.Value);
            printerImpl.WriteLine(value, ccolor);
        }
        public static void WriteLine(string value, string color = null)
        {
            Color? ccolor = null; if (UseColors && color != null) ccolor = ColorFromHtmlSafe(color, Color.DarkGray);
            printerImpl.WriteLine(value, ccolor);
        }
        public static void WriteLine(string value, Color? color = null)
        {
            printerImpl.WriteLine(value, color);
        }
        public static void WriteLine()
        {
            printerImpl.WriteLine();
        }
        public static string PopBuffer()
        {
            return printerImpl.PopBuffer();
        }
        public static void AddAnchor(string anchor_name)
        {
            printerImpl.AddAnchor(anchor_name);
        }

        internal static Color ColorFromHtmlSafe(string color, Color defaultcolor)
        {
            try
            {
                return ColorTranslator.FromHtml(color);
            }
            catch
            {
                return defaultcolor;
            }
        }
    }
}