﻿using EV3ModelLib;
using EV3TreeVisLib;
using EV3TreeVisLib.Printing;
using System;
using System.IO;

namespace Ev3TreeVisApp
{
    class Program
    {
        /// <summary>
        /// Main loop
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            CommandOptions opts = new CommandOptions(args);
            if (opts.DisplayHelp || (string.IsNullOrEmpty(opts.Filename) && !opts.Register))
            {
                // no args given, display help
                CommandOptions.PrintHelp();
                return;
            }
            else if (opts.Register)
            {
                RegisterHelper.Register(args);
                return;
            }

            // process the ev3 file
            string sFileContents = null;
            try
            {
                // check if file exists
                if (!System.IO.File.Exists(opts.Filename))
                {
                    ConsoleExt.WriteLine("Cannot open specified file. File does not exists.", ConsoleColor.Gray);
                    return;
                }
                else
                {
                    Console.Title = $"[EV3TreeVis] {Path.GetFileName(opts.Filename)}";
                    //Console.OutputEncoding = System.Text.Encoding.Default;
                    //ConsoleExt.WriteLine($"OutputEncoding {Console.OutputEncoding.CodePage}, {Console.OutputEncoding.WebName}, InputEncoding {Console.InputEncoding.CodePage}, {Console.InputEncoding.WebName}");
                }

                // prompt on write modification
                if (opts.UpdateProjectSortABC)
                {
                    ConsoleExt.WriteLine("You will modify the file contents. Use this function on you own risk, and backup your file. Can we continue?", ConsoleColor.White);
                    ConsoleExt.WriteLine("  (y=yes, continue with modification | any other key to terminate) ", ConsoleColor.DarkGray);
                    var key = Console.ReadKey(true);
                    if (key.KeyChar != 'y') Environment.Exit(0);
                }

                // iterate endlessy - needed for the list functionality
                while (true)
                {
                    // traverse input file
                    Ev3ZipTraverser zipTraverser = new Ev3ZipTraverser();
                    Node.PrintOptions.SkipPrinting = true; //do not use the new unified Node yet
                    (sFileContents, _, _) = zipTraverser.TraverseZip2(
                        opts.Filename,
                        new Ev3ZipTraverser.OptionsClass
                        {
                            SelectedProgram = opts.BlockToDisplay,
                            OutputMode = opts.OutputMode,
                            ListOnly = opts.ListOnly && string.IsNullOrEmpty(opts.BlockToDisplay),
                            IsTreeForGit = opts.IsTreeForGit,
                            ProjectOnly = opts.ProjectOnly,
                            DebugFlag = opts.DebugFlag,
                            DisplayMetrics = opts.DisplayMetrics,
                            UpdateProjectSortABC = opts.UpdateProjectSortABC
                        });

                    // prompt if listing only
                    if (opts.ListOnly)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine();
                        Console.WriteLine("Enter <blockname> start to display or '.', 'quit', 'exit':");
                        Console.ForegroundColor = ConsoleColor.Gray;

                        string s = Console.ReadLine();
                        opts.BlockToDisplay = s;
                        if (s == "quit" || s == "exit" || s == ".") break;
                    }
                    else
                    {
                        // if not listing, exit loop
                        break;
                    }
                }

                // if outputmode is file based & buffered - create the file
                if (opts.OutputMode != OutputMode.Console)
                {
                    //TODO: add json/yaml handling - challenge: program/myblocks structure
                    string newfilepath = opts.Filename +
                        (opts.OutputMode == OutputMode.HTML ? ".html" : opts.OutputMode == OutputMode.JSON ? ".json" : opts.OutputMode == OutputMode.DOT ? ".dot" : "*.txt");
                    System.IO.File.WriteAllText(newfilepath, sFileContents);
                    ConsoleExt.WriteLine("File " + newfilepath + " created.", ConsoleColor.White);
                }
            }
            catch (Exception e)
            {
                ConsoleExt.WriteLine("Error opening the ev3 file. Sorry for your troubles.\n", ConsoleColor.Gray);
                ConsoleExt.WriteLine(e.ToString(), ConsoleColor.DarkGray);
            }

        }
    }
}
