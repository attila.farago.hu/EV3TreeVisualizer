﻿using Ev3TreeVisApp.Properties;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("EV3 Mindstorms Tree Visualizer")]
[assembly: AssemblyDescription("EV3TreeVis")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("EV3TreeVis")]
[assembly: AssemblyCopyright("Copyright © Attila Farago 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6a04c1a0-665e-4d9c-867e-a120391927f5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
//[assembly: AssemblyVersion("1.0.0.0")]

// using HgMetal for incuding version in the exe file https://bitbucket.org/x0rHamster/hgmetal
[assembly: AssemblyVersion("1")]
[assembly: AssemblyFileVersion(
    "1"
    + "." + HgMetalInfo.CommitWeekYear
    + "." + HgMetalInfo.CommitWeek
    + "." + HgMetalInfo.CommitDay + "0" + HgMetalInfo.CommitHour
)]
[assembly: AssemblyInformationalVersion (
    HgMetalInfo.CommitYear
    + "-" + HgMetalInfo.CommitMonth
    + "-" + HgMetalInfo.CommitDay
    + " [" + HgMetalInfo.CommitTotalHoursSinceRoot + "]"
    + " ("
        + HgMetalInfo.CommitShortId
        + (HgMetalInfo.HasChanges ? "+" + HgMetalInfo.ChangesOrdinalHourQuarter : "")
    + ")"
)]

