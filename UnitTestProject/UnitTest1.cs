﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EV3TreeVisLib;
using EV3TreeVisLib.Printing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        //[ClassInitialize()]
        //public static void ClassInit(TestContext context)
        //{
        //var ev3files = new List<string>() { "filename" };
        //var rootfolder = Path.Combine(context.TestRunDirectory, @"..\..\");
        //foreach (var file in Directory.GetFiles(rootfolder + @"TestFiles\"))
        //{
        //    if (!file.EndsWith(@".ev3")) continue;

        //    ev3files.Add(file);
        //}
        //File.WriteAllLines(rootfolder + "evfiles.csv", ev3files.ToArray());
        //}

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        //readonly List<string> ev3files = new List<string>() {
        //    "brickbutton.ev3",
        //    "colorsensor1.ev3",
        //    "demo_ev3treevis.ev3",
        //    "DINOR3X1.ev3"};
        string TEST_FILES_ROOT = @"..\..\TestFiles"; //EV3s_mindsensors\

        //[DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\ev3files.csv", "ev3files#csv",
        //    DataAccessMethod.Sequential),
        //    DeploymentItem("ev3files.csv")]
        [TestMethod]
        public void TestFilesRootAndRecursive()
        {
            Test1InDirectory(TEST_FILES_ROOT, true);
        }

        [TestMethod]
        public void TestFilesRootOnly()
        {
            Test1InDirectory(TEST_FILES_ROOT, false);
        }

        /// <summary>
        /// Test Files in a directory
        /// For now check only if json serialization and deserialization can be done without exception
        /// </summary>
        /// <param name="path"></param>
        /// <param name="recursive"></param>
        private void Test1InDirectory(string path, bool recursive)
        {
            TestContext.WriteLine("TEST: Check if json serialization and deserialization can be done without exception\n");
            TestContext.WriteLine($"Testing directory: {path}\n");
            var ev3files = Directory.GetFiles(path).Where(fn => fn.EndsWith(".ev3")).ToList();
            foreach (var ev3file in ev3files)
            {
                //var filename = TestContext.DataRow["filename"].ToString();
                TestContext.WriteLine($"Testing {Path.GetFileName(ev3file)}");
                //Console.WriteLine($"Testing {Path.GetFileName(ev3file)}");

                var filename = ev3file; // rootfolder + ev3file;

                // Text test
                Ev3ZipTraverser zipTraverser = new Ev3ZipTraverser();
                var outputText = zipTraverser.TraverseZip(filename,
                    new Ev3ZipTraverser.OptionsClass
                    {
                        HandleAndReportErrors = false,
                        OutputMode = OutputMode.Text
                    });

                // JSON test
                var output = zipTraverser.TraverseZip(filename,
                    new Ev3ZipTraverser.OptionsClass
                    {
                        HandleAndReportErrors = false,
                        OutputMode = OutputMode.JSON
                    });
                JArray json = JArray.Parse(output);
                var programs = json.Select(elem => elem["name"].ToString()).ToArray();

                TestContext.WriteLine($"Tests passed for files: {Path.GetFileName(ev3file)} | " + string.Join(", ", programs) + "\n");

                if (recursive)
                {
                    Directory.GetDirectories(path).ToList().ForEach(dir => Test1InDirectory(dir, recursive));
                }
            }
        }
    }
}
